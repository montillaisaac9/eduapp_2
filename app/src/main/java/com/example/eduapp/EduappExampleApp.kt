package com.example.eduapp

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import dagger.hilt.android.HiltAndroidApp
import com.jakewharton.threetenabp.AndroidThreeTen


@HiltAndroidApp
class EduappExampleApp:Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        createNotificationChannel()
    }

    companion object {
        const val NOTIFICATION_CHANNEL_ID = "notification_fcm"
    }

    private fun createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "example",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.description = "esto es una prueba"
            val notificationChannel = getSystemService(NotificationManager::class.java)
            notificationChannel.createNotificationChannel(channel)
        }
    }

}