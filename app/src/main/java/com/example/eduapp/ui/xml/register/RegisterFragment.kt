package com.example.eduapp.ui.xml.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.eduapp.R
import com.example.eduapp.databinding.FragmentRegisterBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private lateinit var registerViewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerViewModel = ViewModelProvider(this)[RegisterViewModel::class.java]

        val bgmain = binding.imageBg
        val imgIconBg = R.drawable.img

        Glide.with(this)
            .load(imgIconBg)
            .centerCrop()
            .into(bgmain)

        val imgIconMain = binding.imageICONMain
        val imgIconMainbg = R.drawable.logo_size

        Glide.with(this)
            .load(imgIconMainbg)
            .fitCenter()
            .into(imgIconMain)
        // Inflate the layout for this fragment

        initEvents()
        subcribeObserver()
    }

    private fun initEvents(){

        binding.btnRegister.setOnClickListener { verifyUser() }

        val inciarLink = binding.t2IniciarSecion

        inciarLink.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment2)
            inciarLink.isEnabled = false
        }

    }

    private fun subcribeObserver (){

        registerViewModel.isLoadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        registerViewModel.userLiveData.observe(viewLifecycleOwner){ success ->
                if (success) {
                    val email = binding.editTextEmail.text.toString()
                    val pass1 = binding.editTextPassword.text.toString()
                    val rol = binding.swichtRole.isChecked()

                    val bundle = Bundle().apply {
                        putString("email", email)
                        putString("password", pass1)
                    }

                    if (rol){
                        Toast.makeText(this.context, "complete su registro como profesor", Toast.LENGTH_SHORT).show()
                        findNavController().navigate(R.id.action_registerFragment_to_completarPerfilFragmentPr, bundle)
                    }else {
                        Toast.makeText(this.context, "complete su registro como estudiante", Toast.LENGTH_SHORT).show()
                        findNavController().navigate(R.id.action_registerFragment_to_completarPerfilFragmentES, bundle)
                    }

                }
            }

        registerViewModel.errorLiveData.observe(viewLifecycleOwner) { text ->
            if (text != "") Toast.makeText(this.context, text, Toast.LENGTH_SHORT).show()
        }
    }

    private fun verifyUser(){
        val email = binding.editTextEmail.text.toString()
        val pass1 = binding.editTextPassword.text.toString()
        val pass2 = binding.ConftimEditTextPassword.text.toString()
        val rol = binding.swichtRole.isChecked()

        if (email.isEmpty()){
            Toast.makeText(this.context, "el campo email esta vacio, rellenelo por favor", Toast.LENGTH_SHORT).show()
        } else if (pass1.isEmpty()){
            Toast.makeText(this.context, "el campo de la contraseña 1 esta vacio, rellenelo por favor", Toast.LENGTH_SHORT).show()
        } else if (pass2.isEmpty()){
            Toast.makeText(this.context, "el campo de confirmacion de la contraseña esta vacio, rellenelo por favor", Toast.LENGTH_SHORT).show()
        } else registerViewModel.verifiyUser(email, pass1, pass2, rol, requireContext())
    }
}