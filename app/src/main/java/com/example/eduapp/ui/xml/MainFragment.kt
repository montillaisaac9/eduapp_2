package com.example.eduapp.ui.xml

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.eduapp.R
import com.example.eduapp.databinding.FragmentMainBinding
import com.example.eduapp.ui.xml.login.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val view = binding.root

        // Puedes acceder a las vistas usando binding.viewId

        val bgmain = binding.imageBg
        val imgIconBg = R.drawable.img

        Glide.with(this)
            .load(imgIconBg)
            .centerCrop()
            .into(bgmain)

        val imgIconMain = binding.imageICONMain
        val imgIconMainbg = R.drawable.logo_size

        Glide.with(this)
            .load(imgIconMainbg)
            .fitCenter()
            .into(imgIconMain)

        val btn_login = binding.btnLogin


        btn_login.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_loginFragment)
            btn_login.isEnabled = false
        }

        loginViewModel.getUserFromDatabase()


        loginViewModel.studentLoginResultLiveData.observe(viewLifecycleOwner) { success ->
            if (success) {
                Toast.makeText(this.context, "ah iniciado sesion con exito estudiante", Toast.LENGTH_SHORT)
                    .show()
                findNavController().navigate(R.id.action_mainFragment_to_compose_fragment_es)
            } else {
                Toast.makeText(this.context, "El usuario no esta registrado", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        loginViewModel.profesorLoginResultLiveData.observe(viewLifecycleOwner) { success ->
            if (success) {
                Toast.makeText(this.context, "ah iniciado sesion con exito como profesor", Toast.LENGTH_SHORT)
                    .show()
                findNavController().navigate(R.id.action_mainFragment_to_compose_fragment_pr)
            } else {
                Toast.makeText(this.context, "El usuario no esta registrado", Toast.LENGTH_SHORT)
                    .show()
            }
        }


        val registerLink = binding.t2register

        registerLink.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_registerFragment)
            registerLink.isEnabled = false
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null // Desenlaza el binding cuando el fragmento se destruye para evitar fugas de memoria
    }
}