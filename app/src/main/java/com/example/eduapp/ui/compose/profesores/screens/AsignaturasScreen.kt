package com.example.eduapp.ui.compose.profesores.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.eduapp.R
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.profesores.navigationProfesor.ProfesorScreen
import com.example.eduapp.ui.compose.untils_ui.CustomButton
import com.example.eduapp.ui.compose.untils_ui.MaterialButton
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AsignaturasScreen(
    userInfoState: List<UserProfesorEntity>,
    profesorviewModel: ViewModelProfesor,
    navController: NavController
) {

    var scope = rememberCoroutineScope()
    val context = LocalContext.current

    var id by remember { mutableStateOf(0) }

    LaunchedEffect(userInfoState) {
        userInfoState.forEach { profesor ->
            id = profesor.id
        }
    }

    LaunchedEffect(id) {
        if (id != 0) {
            profesorviewModel.getAsignatures(id, context)
        }
    }

    val asignaturas by profesorviewModel.asignaturas.collectAsState(initial = emptyList())


    var name by remember { mutableStateOf("") }
    var description by remember { mutableStateOf("") }
    val state = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        skipHalfExpanded = true
    )

    ModalBottomSheetLayout(
        sheetState = state,

        sheetContent = {
            Column(modifier = Modifier.size(600.dp)) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(50.dp)
                        .background(
                            color = colorResource(id = R.color.white),
                            shape = RoundedCornerShape(20.dp)
                        ),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    CustomButton(
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(end = 16.dp),
                        onClick = {
                            name = ""
                            description = ""
                            scope.launch {
                                state.hide()
                            }
                        },
                        icon = Icons.Default.ArrowBack // Aquí pasas el icono que desees
                    )
                    Text(
                        text = "Crea tu asignatura",
                        style = TextStyle(fontSize = 32.sp),
                        modifier = Modifier.padding(16.dp)
                    )
                }
                Text(
                    text = "nombre de la materia:",
                    style = TextStyle(fontSize = 18.sp),
                    modifier = Modifier
                        .padding(start = 25.dp)
                        .fillMaxWidth()
                )
                TextField(
                    value = name, onValueChange = { name = it }, modifier = Modifier
                        .padding(start = 25.dp, end = 25.dp)
                        .fillMaxWidth(),
                    maxLines = 1,
                    singleLine = true,
                    colors = TextFieldDefaults.textFieldColors(
                        cursorColor = colorResource(id = R.color.orange), // Color del cursor
                        focusedIndicatorColor = colorResource(id = R.color.orange), // Color de la línea de base cuando el TextField está enfocado
                        unfocusedIndicatorColor = colorResource(id = R.color.gray) // Color de la línea de base cuando el TextField no está enfocado
                    )
                )

                Text(
                    text = "descripcion de la materia:",
                    style = TextStyle(fontSize = 18.sp),
                    modifier = Modifier
                        .padding(start = 25.dp, top = 30.dp)
                        .fillMaxWidth()
                )
                TextField(
                    value = description,
                    onValueChange = { description = it },
                    modifier = Modifier
                        .padding(start = 25.dp, end = 25.dp)
                        .fillMaxWidth(),
                    maxLines = 1,
                    singleLine = true,
                    colors = TextFieldDefaults.textFieldColors(
                        cursorColor = colorResource(id = R.color.orange), // Color del cursor
                        focusedIndicatorColor = colorResource(id = R.color.orange), // Color de la línea de base cuando el TextField está enfocado
                        unfocusedIndicatorColor = colorResource(id = R.color.gray) // Color de la línea de base cuando el TextField no está enfocado
                    )
                )

                MaterialButton(text = "Guardar") {
                    if (description.isEmpty() or name.isEmpty())Toast.makeText(context, "rellene los campos", Toast.LENGTH_SHORT).show()
                    else {
                        guardarMateria(name, description, id, context, profesorviewModel)
                        name = ""
                        description = ""
                        scope.launch {
                            state.hide()
                        }
                    }

                }
            }
        },
        sheetShape = RoundedCornerShape(topStart = 30.dp, topEnd = 30.dp)
    ) {
        Column(Modifier.fillMaxSize()) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .background(color = colorResource(id = R.color.white)),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Asignaturas",
                    style = TextStyle(fontSize = 32.sp),
                    modifier = Modifier.padding(16.dp)
                )
                CustomButton(
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(end = 16.dp),
                    onClick = {
                        scope.launch {
                            state.show()
                        }
                    },
                    icon = Icons.Default.Add // Aquí pasas el icono que desees
                )
            }
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                items(asignaturas.size) { index ->
                    val asignatura = asignaturas[index]
                    asignatura?.let { ListItemRow(it, context, navController) }
                }
            }
        }
    }
}


@Composable
fun ListItemRow(asignatura: AsignaturaDomain, context: Context, navController: NavController) {
    val coroutineScope = rememberCoroutineScope()
    var clickableEnabled by remember { mutableStateOf(true) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(120.dp)
            .padding(horizontal = 20.dp, vertical = 20.dp)
            .background(
                color = colorResource(id = R.color.orange),
                shape = RoundedCornerShape(8.dp) // Agregar la forma redondeada
            )
            .clickable {
                if (clickableEnabled) {
                    coroutineScope.launch {
                        clickableEnabled = false // Desactivar el clic
                        navController.navigate("${ProfesorScreen.DetaillesAsignaturaScreen.routes}/${asignatura.id}")
                        Toast
                            .makeText(context, asignatura.nombre, Toast.LENGTH_SHORT)
                            .show()
                        delay(1500L)
                        clickableEnabled = true
                    }
                }
            },
        contentAlignment = Alignment.Center,

        ) {
        Text(
            text = asignatura.nombre,
            textAlign = TextAlign.Center, // Centrar el texto horizontalmente
            modifier = Modifier.fillMaxWidth(),
            style = TextStyle(fontSize = 24.sp)// Asegurar que el texto ocupe todo el ancho
        )
    }
}


fun guardarMateria(
    name: String,
    description: String,
    id: Int,
    context: Context,
    profesorviewModel: ViewModelProfesor
): Boolean {
    if (name.isEmpty()) Toast.makeText(
        context,
        "el nombre no puede ser vacio",
        Toast.LENGTH_SHORT
    ).show()
    else if (description.isEmpty()) Toast.makeText(
        context,
        "la descripcion no puede ser vacio",
        Toast.LENGTH_SHORT
    ).show()
    else {
        profesorviewModel.crearAsignatura(name, description, id, context)
    }
    return true
}