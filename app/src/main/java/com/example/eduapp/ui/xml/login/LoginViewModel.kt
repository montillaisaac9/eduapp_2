package com.example.eduapp.ui.xml.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eduapp.domain.auth.UseCaseAuth
import com.example.eduapp.untils.isOnline
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val useCaseAuth: UseCaseAuth
): ViewModel() {

    val studentLoginResultLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val profesorLoginResultLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val itsEmpy: MutableLiveData<Boolean> = MutableLiveData()
    val istOnline: MutableLiveData<Boolean> = MutableLiveData()
    val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun login_S (email: String, password: String){
        viewModelScope.launch {
            val result = useCaseAuth.loginS(email, password)
            Log.i("inicio", "logrado")
            if (!result.isNullOrEmpty()){
                studentLoginResultLiveData.postValue(!result.isNullOrEmpty())
                isLoadingLiveData.postValue(false)
            }
            else {
                studentLoginResultLiveData.postValue(false)
                isLoadingLiveData.postValue(false)
            }
        }
    }

    fun login_P (email: String, password: String){
        viewModelScope.launch {
            val result = useCaseAuth.loginP(email, password)
            Log.i("inicio", "logrado")
            if (!result.isNullOrEmpty()){
                profesorLoginResultLiveData.postValue(!result.isNullOrEmpty())
                isLoadingLiveData.postValue(false)
            }
            else {
                profesorLoginResultLiveData.postValue(false)
                isLoadingLiveData.postValue(false)
            }
        }
    }

    fun loginAuth(email: String, pass: String, rol: Boolean, requireContext: Context) {
        if (isOnline(requireContext)) {
            istOnline.postValue(false)
            if (email.isNotEmpty() && pass.isNotEmpty()) {
                isLoadingLiveData.postValue(true)
                if (rol) {
                    login_P(email, pass)
                } else {
                    login_S(email, pass)
                }
            } else {
                itsEmpy.postValue(true)
            }
        } else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
        }
    }
    fun getUserFromDatabase(){
            viewModelScope.launch {
                useCaseAuth.getStudent().collect(){ it ->
                    if (it.isNotEmpty()){
                        studentLoginResultLiveData.postValue(true)
                    }
                }
                viewModelScope.launch { useCaseAuth.getProfesor().collect { it ->
                    if (it.isNotEmpty()){
                        profesorLoginResultLiveData.postValue(true)
                    }
                }
                }
            }
    }
}
