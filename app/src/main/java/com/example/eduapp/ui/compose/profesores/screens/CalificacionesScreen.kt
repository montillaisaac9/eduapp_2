package com.example.eduapp.ui.compose.profesores.screens

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Done
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.eduapp.R
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.dataToView.NotasWithName
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose


@Composable
fun CalificacionesScreenProfesor(
    profesorviewModel: ViewModelProfesor,
    idAsignatura: Int?,
    idActividad: Int?,
    navController: NavHostController
) {

    val asignaturaS by profesorviewModel.asignaturaDetails.collectAsState(initial = null)
    val actividadS by profesorviewModel.activityDetails.collectAsState(initial = null)
    val notas by profesorviewModel.notasConNombre.collectAsState()

    Log.e("notas", notas.toString())


    LaunchedEffect(idAsignatura) {
        if (idAsignatura != 0) {
            idAsignatura?.let { profesorviewModel.getAsignaturaById(it)}
        }
    }

    val context = LocalContext.current

    LaunchedEffect(idActividad) {
        if (idActividad != 0) {
            idActividad?.let { asignaturaS?.let { it1 ->
                profesorviewModel.getNotas(idActividad,
                    it1, context)
                profesorviewModel
            } }
            idActividad?.let { profesorviewModel.getActivityById(it) }
        }
    }

    val isLoading by profesorviewModel.isLoadingFlow.collectAsState(initial = false)

    if (isLoading) {
        ProgressBarCompose()
    } else {
        Content(profesorviewModel, idActividad, idAsignatura, navController, actividadS, asignaturaS, notas)
    }


}
@Composable
fun Content(
    profesorviewModel: ViewModelProfesor,
    idActividad: Int?,
    idAsignatura: Int?,
    navController: NavHostController,
    actividadS: ActividadDomain?,
    asignaturaS: AsignaturaDomain?,
    notas: List<NotasWithName>
) {

    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
            .background(color = colorResource(id = R.color.white)),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "Calificacion del ${actividadS?.nombre} de la ${asignaturaS?.nombre}",
            modifier = Modifier
                .padding(top = 30.dp),
            style = TextStyle(fontSize = 20.sp)
        )
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(notas.size) { index ->
                val notas = notas[index]
                ListItemRowNotas(notas, index, asignaturaS, context, profesorviewModel, actividadS)
            }
        }
    }
}

@Composable
fun ListItemRowNotas(
    notas: NotasWithName,
    index: Int,
    asignaturaS: AsignaturaDomain?,
    context: Context,
    profesorviewModel: ViewModelProfesor,
    actividadS: ActividadDomain?
) {
    var counter = index + 1

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 6.dp, horizontal = 10.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Modifier
                .size(40.dp)
                .background(
                    color = colorResource(id = R.color.orange),
                    shape = CircleShape
                ),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = counter.toString(),
                textAlign = TextAlign.Center,
                style = TextStyle(fontSize = 18.sp, color = Color.White)
            )
        }

        Spacer(modifier = Modifier.width(8.dp))

        Text(
            text = notas.nombre,
            modifier = Modifier
                .weight(2f)
                .padding(horizontal = 6.dp),
            style = TextStyle(fontSize = 16.sp)
        )

        Spacer(modifier = Modifier.width(8.dp))

        if (notas.puntuacion == null) {
            var puntuacion by remember { mutableStateOf("") }

            OutlinedTextField(
                value = puntuacion,
                onValueChange = { puntuacion = it },
                enabled = true,
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 6.dp),
                label = { Text(text = "Nota") },
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                singleLine = true,
                textStyle = TextStyle(fontSize = 16.sp),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    textColor = colorResource(id = R.color.black),
                    backgroundColor = colorResource(id = R.color.white),
                    focusedBorderColor = colorResource(id = R.color.orange_dark),
                    unfocusedBorderColor = colorResource(id = R.color.orange),
                    focusedLabelColor = colorResource(id = R.color.orange),
                    cursorColor = colorResource(id = R.color.orange),
                )
            )
            Spacer(modifier = Modifier.width(8.dp))
            IconButton(
                onClick = {
                    editarNota(notas, puntuacion, context, asignaturaS, actividadS, profesorviewModel)
                },
                modifier = Modifier
                    .size(32.dp)
                    .background(
                        color = colorResource(id = R.color.orange),
                        shape = CircleShape
                    ),
            ) {
                Icon(
                    imageVector = Icons.Rounded.Done,
                    contentDescription = "Guardar",
                    tint = Color.White,
                    modifier = Modifier.size(20.dp)
                )
            }
        } else {
            Text(
                text = notas.puntuacion.toString(),
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 6.dp),
                style = TextStyle(fontSize = 16.sp)
            )
        }
    }
}

fun editarNota(
    notas: NotasWithName,
    puntuacion: String,
    context: Context,
    asignaturaS: AsignaturaDomain?,
    actividadS: ActividadDomain?,
    profesorviewModel: ViewModelProfesor
) {
    if (puntuacion.isEmpty()){
        Toast.makeText(context, "la Nota no puede estar vacia", Toast.LENGTH_SHORT).show()
    } else {
        if (puntuacion.toInt() > actividadS!!.puntuacion_Max || puntuacion.toInt() <= 0){
            Toast.makeText(context, "la Nota no puede ser mayor a ${actividadS.puntuacion_Max} o menor a 0", Toast.LENGTH_SHORT).show()
        } else profesorviewModel.editarNota(notas, puntuacion, context, asignaturaS)
    }

}
