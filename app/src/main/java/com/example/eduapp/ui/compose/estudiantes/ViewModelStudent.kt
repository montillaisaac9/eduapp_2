package com.example.eduapp.ui.compose.estudiantes

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.example.eduapp.domain.auth.UseCaseAuth
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.NewNotaDomain
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.StudentDomain
import com.example.eduapp.domain.model.toDomain
import com.example.eduapp.domain.student.UseCaseEs
import com.example.eduapp.ui.compose.dataToView.NotasEstadisticas
import com.example.eduapp.untils.isOnline
import com.google.firebase.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.messaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.forEach
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Period
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject



@HiltViewModel
class ViewModelStudent @Inject constructor(
    private val useCaseAuth: UseCaseAuth,
    private val studentUseCase: UseCaseEs
) : ViewModel() {

    private val _user = MutableStateFlow<List<StudentDomain>>(emptyList())
    val user: StateFlow<List<StudentDomain>> = _user

    private var _asignaturas = MutableStateFlow<List<AsignaturaDomain?>>(emptyList())
    val asignaturas: StateFlow<List<AsignaturaDomain?>> = _asignaturas

    private var _asignaturaDetails: MutableStateFlow<AsignaturaDomain?> = MutableStateFlow(null)
    val asignaturaDetails: StateFlow<AsignaturaDomain?> = _asignaturaDetails

    private var _actividades = MutableStateFlow<List<ActividadDomain?>>(emptyList())
    val actividades: StateFlow<List<ActividadDomain?>> = _actividades

    val isLoadingFlow: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val istOnline: MutableLiveData<Boolean> = MutableLiveData()

    private var _notaCreada = MutableStateFlow(false)
    val notaCreada: StateFlow<Boolean> = _notaCreada

    private var _notaMostrar = MutableStateFlow<List<NotaDomain?>>(emptyList())
    var notaMostrar: MutableStateFlow<List<NotaDomain?>> = _notaMostrar


    private val _notasStadisticas = MutableStateFlow<List<NotasEstadisticas>>(emptyList())
    val notasStadisticas: StateFlow<List<NotasEstadisticas>> = _notasStadisticas


    fun setLoading(isLoading: Boolean) {
        isLoadingFlow.value = isLoading
    }

    fun getUser() {
        viewModelScope.launch {
            useCaseAuth.getStudent().collect { list ->
                var estudiante: List<StudentDomain>

                val stududiante = list.map {
                    it.toDomain()
                }
                stududiante.forEach{
                    estudiante = listOf(StudentDomain(it.id, it.nombre, it.correo, it.contraseña, it.fechaNacimiento, it.foto, it.sexo, it.telefono, it.latitud, it.longitud,it.materias, edad = calcularEdad(it.fechaNacimiento)))
                    _user.value = estudiante
                }
            }
        }
    }

    fun logout() {
        viewModelScope.launch {
            useCaseAuth.logOut(false)
        }
    }

    fun getAsignaturesForInscripcion(context: Context) {
        if (isOnline(context)) {
            viewModelScope.launch {
                studentUseCase.getAllasignatura().flatMapLatest { asignaturas ->
                    user.map { users ->
                        val inscritas =
                            users.flatMap { it.materias } // Obtener todas las asignaturas inscritas de todos los usuarios
                        val disponibles = asignaturas.filterNot { asignatura ->
                            inscritas.any { it.id == asignatura?.id } // Filtrar las asignaturas que ya están inscritas
                        }
                        disponibles
                    }
                }.collect { disponibles ->
                    _asignaturas.value = disponibles
                }
            }
        } else {
            viewModelScope.launch {
                studentUseCase.getAllasignatura().collect { asignaturas ->
                    _asignaturas.value = asignaturas
                }
            }
        }
    }


    fun inscripcionVm(
        idUser: String,
        nameUser: String,
        edadUser: String,
        asignatura: AsignaturaDomain,
        asignaturasInscritas: List<MateriaInscita>,
        context: Context,
        currentDate: LocalDateTime,
        sexo_user: String
    ) {
        if (isOnline(context)) {
        Firebase.messaging.token.addOnCompleteListener {
            if (!it.isSuccessful) Log.e("error al obtener el token", "token no generado")
            else {
                val token = it.result
                Log.e("token generado Correctamente", token)
                val estudianteInscrito =
                    EstudianteInscrito(idUser, nameUser, edadUser, sexo_user, currentDate, token, null)
                val materiasInscritas = MateriaInscita(asignatura.id, asignatura.nombre, asignatura.profesorId, asignatura.descripcion)

                val aS = asignaturasInscritas.toMutableList()
                aS.add(materiasInscritas)
                var materiasInscritasList = aS.toList()

                val asignaturas: StateFlow<List<AsignaturaDomain?>> = _asignaturas

                var estudianteInscritoMl: MutableList<EstudianteInscrito>
                setLoading(true)
                viewModelScope.launch {
                    asignaturas.collect { asignaturasList ->
                        asignaturasList.forEach { asignatura ->
                            if (asignatura!!.id == materiasInscritas.id) {
                                estudianteInscritoMl =
                                    asignatura.estudiantes?.toMutableList() ?: mutableListOf()
                                estudianteInscritoMl.add(estudianteInscrito)

                                val aR = InsStudentsRe(materiasInscritasList)
                                val sR = InsAsignaturaRe(estudianteInscritoMl)

                                setLoading(true)

                                    // Llama a la función de inscripción
                                studentUseCase.inscripcion(aR, idUser, sR, materiasInscritas.id)

                                    // Después de la inscripción, refresca los datos del usuario
                                studentUseCase.refrescar(idUser).collect {list ->
                                    var estudiante: List<StudentDomain>

                                    val stududiante = list.map {
                                        it.toDomain()
                                    }
                                    stududiante.forEach{
                                        estudiante = listOf(StudentDomain(it.id, it.nombre, it.correo, it.contraseña, it.fechaNacimiento, it.foto, it.sexo, it.telefono, it.latitud, it.longitud,it.materias, edad = calcularEdad(it.fechaNacimiento)))
                                        _user.value = estudiante
                                    }
                                        setLoading(false)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            istOnline.postValue(false)
        }
    }

    fun getActivities(id: Int, context: Context) {
        Log.i("id", id.toString())
        if (isOnline(context)) {
            setLoading(true)
            istOnline.postValue(true)
            viewModelScope.launch {
                studentUseCase.getActividadByMateria(id).collect { it ->
                    Log.d("Actividades", it.toString())
                    _actividades.value = it
                }
            }
            setLoading(false)
        } else {
            istOnline.postValue(false)
        }
    }

    fun getAsignaturaById(id: Int) {
        viewModelScope.launch {
            studentUseCase.getAsignaturaById(id).collect { it ->
                _asignaturaDetails.value = it
            }
        }
    }

    fun crearNota(
        id: Int,
        asignaturaId: Int,
        idUser: String,
        formattedDate: String,
        context: Context
    ) {

        val newNota = NewNotaDomain(
            studentId = idUser,
            asignaturaId = asignaturaId,
            actividadId = id,
            puntuacion = null,
            fecha = formattedDate
        )
        if (isOnline(context)) {
            viewModelScope.launch {
                val isCreated = studentUseCase.verificarNota(idUser, asignaturaId, id)
                if (!isCreated) {
                    setLoading(true)
                    studentUseCase.crearNota(newNota)
                } else {
                    _notaCreada.value = true
                    delay(2000L)
                    _notaCreada.value = false
                }
                setLoading(false)
            }
        } else {
            istOnline.postValue(false)
        }
    }


    fun getNotasStadisticas(asignaturaS: AsignaturaDomain, context: Context) {
        if (isOnline(context)) {
            setLoading(true)
            val estudiantesMateria = asignaturaS.estudiantes

            viewModelScope.launch {
                studentUseCase.getStadictsUc(asignaturaS.id).collect { notas ->
                    val notasConNombreSet = mutableSetOf<NotasEstadisticas>()

                    notas.forEach { nota ->
                        estudiantesMateria?.find { it.id == nota?.studentId }?.let { estudiante ->
                            val nombre = estudiante.nombre
                            val fechaNacimiento = estudiante.fecha
                            val sexo = estudiante.sexo


                            val edad = calcularEdad("${fechaNacimiento}")

                            // Crear un objeto NotasWithName incluyendo la edad y el sexo
                            val notasEstadistica = NotasEstadisticas(
                                asignaturaId = nota!!.asignaturaId,
                                puntuacion = nota.puntuacion,
                                edad = edad,
                                sexo = sexo
                            )

                            // Añadir a la lista de notas con nombre
                            notasConNombreSet.add(notasEstadistica)
                        }
                    }

                    _notasStadisticas.value =
                        notasConNombreSet.toList() // Convertimos a lista para la asignación
                    Log.e("mutable_list", _notasStadisticas.value.toString())
                    setLoading(false)
                }
            }
        } else {
            istOnline.postValue(false)
        }
    }


    fun calcularEdad(fechaNacimiento: String): Int {
        // Define el formato de fecha en formato "dd-MM-yyyy"
        val formato = DateTimeFormatter.ofPattern("dd-MM-yyyy")

        // Convierte la fecha de nacimiento de tipo String a LocalDate
        val fechaNacimientoDate = LocalDate.parse(fechaNacimiento, formato)

        // Obtiene la fecha actual
        val fechaActual = LocalDate.now()

        // Calcula la edad
        val edad = Period.between(fechaNacimientoDate, fechaActual).years
        Log.e("edad", edad.toString())
        return edad
    }

    fun obtenerNota(
        id: Int,
        asignaturaId: Int,
        idUser: String,
        formattedDate: String,
        context: Context
    ) {
        if (isOnline(context)) {
            setLoading(true)
            viewModelScope.launch {
                val nota = studentUseCase.obtenerNotas(idUser, asignaturaId, id)
                nota.collect {
                    Log.e("nota viewmodel", nota.toString())
                    notaMostrar.value = it
                }
            }
            setLoading(false)
        } else istOnline.postValue(false)
    }

/*
    fun sendNotificationToDevice(token: String, title: String, body: String) {
        val message = Message.builder()
            .putData("title", title)
            .putData("body", body)
            .setToken(token)
            .build()

        FirebaseMessaging.getInstance().send(message)
    }
*/

}
