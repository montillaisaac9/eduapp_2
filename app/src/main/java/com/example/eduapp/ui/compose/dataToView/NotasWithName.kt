package com.example.eduapp.ui.compose.dataToView

data class NotasWithName(
    val id: Int,
    val studentId : String,
    val asignaturaId: Int,
    val actividadId: Int,
    val puntuacion: Int?,
    val fecha: String,
    val nombre: String,
    val edad: Int,
    val sexo: String
)
