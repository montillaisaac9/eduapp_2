package com.example.eduapp.ui.compose.estudiantes.navigationStudent

sealed class StudentScreens (val routes: String) {
    object studentAsignaturaScreen: StudentScreens("asignaturaEstudiantes")
    object studentActividadScreen: StudentScreens("actividadesEstudiantes")
    object studentEstadisticasScreen: StudentScreens("estaditicasEstudiantes")
}