package com.example.eduapp.ui.compose.profesores

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.example.eduapp.domain.auth.UseCaseAuth
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.domain.profesor.UseCasePr
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.NewActivityDomain
import com.example.eduapp.domain.model.NewAsignaturaDomain
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.ui.compose.dataToView.NotasEstadisticas
import com.example.eduapp.ui.compose.dataToView.NotasWithName
import com.example.eduapp.untils.isOnline
import com.google.firebase.Firebase
import com.google.firebase.messaging.messaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate
import org.threeten.bp.Period
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class ViewModelProfesor @Inject constructor(
    private val useCaseAuth: UseCaseAuth,
    private val useCasePr: UseCasePr
): ViewModel() {


    private val _user = MutableStateFlow<List<UserProfesorEntity>>(emptyList())
    val user: StateFlow<List<UserProfesorEntity>> = _user

    private var _asignaturas = MutableStateFlow<List<AsignaturaDomain?>>(emptyList())
    val asignaturas: StateFlow<List<AsignaturaDomain?>> = _asignaturas

    private var _asignaturaDetails: MutableStateFlow<AsignaturaDomain?> = MutableStateFlow(null)
    val asignaturaDetails: StateFlow<AsignaturaDomain?> = _asignaturaDetails

    private var _activityDetails: MutableStateFlow<ActividadDomain?> = MutableStateFlow(null)
    val activityDetails: StateFlow<ActividadDomain?> = _activityDetails

    private var _detallesEstudiante = MutableStateFlow<List<UserStudentEntity>>(emptyList())
    val detallesEstudiante: StateFlow<List<UserStudentEntity>> = _detallesEstudiante


    private var _listaFiltrada: MutableStateFlow<List<EstudianteInscrito>> = MutableStateFlow(emptyList())
    val listaFiltrada: StateFlow<List<EstudianteInscrito>> = _listaFiltrada

    private var _actividades = MutableStateFlow<List<ActividadDomain?>>(emptyList())
    val actividades: StateFlow<List<ActividadDomain?>> = _actividades

    private val _notasConNombreEstudianteFlow = MutableStateFlow<List<NotasWithName>>(emptyList())
    val notasConNombreEstudianteFlow: StateFlow<List<NotasWithName>> = _notasConNombreEstudianteFlow

    private val _notasStadisticas = MutableStateFlow<List<NotasEstadisticas>>(emptyList())
    val notasStadisticas: StateFlow<List<NotasEstadisticas>> = _notasStadisticas


    private val _notasConNombre = MutableStateFlow<List<NotasWithName>>(emptyList())
    val notasConNombre: StateFlow<List<NotasWithName>> = _notasConNombre

    val istOnline: MutableLiveData<Boolean> = MutableLiveData()

    val errorLiveData: MutableLiveData<String> = MutableLiveData()

    val isLoadingFlow: MutableStateFlow<Boolean> = MutableStateFlow(false)

    fun setLoading(isLoading: Boolean) {
        isLoadingFlow.value = isLoading
    }

    fun getUser() {
        viewModelScope.launch {
            useCaseAuth.getProfesor().collect {
                _user.value = it
            }
        }
    }

    fun logout(){
            viewModelScope.launch {
                useCaseAuth.logOut(true)
            }
    }

    fun crearAsignatura(name: String, description: String, id: Int, context: Context) {
        if (isOnline(context)){
            Firebase.messaging.token.addOnCompleteListener {
                if (!it.isSuccessful) Log.e("error al obtener el token", "token no generado")
                else {
                    val token = it.result
                    Log.e("token generado para profesor", token)
                    val newAsignaturaDomain = NewAsignaturaDomain(
                        nombre = name,
                        descripcion = description,
                        profesorId = id,
                        estudiantesIds = emptyList(),
                        token = token
                    )


                    setLoading(true)
                    viewModelScope.launch {
                        useCasePr.createSignature(newAsignaturaDomain)
                        getAsignatures(id, context)
                    }
                    setLoading(false)
                }

            }
        }
        else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
        }
    }
    fun getAsignatures(id: Int, context: Context) {
        if (isOnline(context)) {
            setLoading(true)
            istOnline.postValue(true)
            viewModelScope.launch {
                useCasePr.getAsignature(id).collect{ it ->
                    _asignaturas.value = it
                }
            }
            setLoading(false)
        } else {
            istOnline.postValue(false)
        }
    }

    fun getAsignaturaById(id: Int){
        viewModelScope.launch {
            useCasePr.gerAsignaturaById(id).collect{ it ->
                _asignaturaDetails.value = it
            }
        }
    }

    fun getStudent(id: String, context: Context) {
        if (isOnline(context)) {
            istOnline.postValue(true)
            setLoading(true)
            viewModelScope.launch {
                useCasePr.getStudentById(id).collect{ it ->
                _detallesEstudiante.value = it
                setLoading(false)
                }
            }
        } else {
            _detallesEstudiante.value = emptyList()
            istOnline.postValue(false)
        }
    }

    fun delete(it: EstudianteInscrito, asignaturaDomain: AsignaturaDomain?, context: Context) {
        if (isOnline(context)) {
            setLoading(true)
            istOnline.postValue(true)
            var estudianteList = asignaturaDomain?.estudiantes?.toMutableList()
            estudianteList?.remove(it)

            val Ar = estudianteList?.let { it1 -> InsAsignaturaRe(it1.toList()) }

            viewModelScope.launch {
                var listaMaterias: MutableList<MateriaInscita> = mutableListOf()

                // Obtener los estudiantes y asignaturas
                val students = useCasePr.getStudentById(it.id).first()

                students.forEach { student ->
                    Log.d("estudiante", student.materias.toString())
                    listaMaterias = student.materias.toMutableList()
                }

                // Crear la materia a eliminar
                val materiaEliminar = asignaturaDomain?.let { it1 ->
                    MateriaInscita(
                        id = it1.id,
                        nombre = asignaturaDomain.nombre,
                        descripcion = asignaturaDomain.descripcion,
                        profesorId = asignaturaDomain.profesorId
                    )
                }

                // Eliminar la materia de la lista
                listaMaterias.remove(materiaEliminar)
                val Sr = InsStudentsRe(listaMaterias.toList())

                // Eliminar al estudiante
                useCasePr.deletedEsAs(Sr, it.id, Ar!!, asignaturaDomain!!.id)

                // Obtener las asignaturas actualizadas
                useCasePr.getAsignature(asignaturaDomain.profesorId).collect { asignaturas ->
                    _asignaturas.value = asignaturas
                    setLoading(false)
                }
            }
        } else {
            istOnline.postValue(false)
        }
    }

    fun crearActivity(name: String, description: String, maxPoints: String, fechaEntrega: String, id: Int, context: Context){
        val newActividad = NewActivityDomain(
            asignatura_id = id,
            nombre = name,
            descripcion = description,
            puntuacion_Max = maxPoints.toInt(),
            fecha_limit = fechaEntrega,
        )

        if (isOnline(context)){
            setLoading(true)
            viewModelScope.launch {
                useCasePr.createActivity(newActividad)
                getAsignatures(id, context)
                useCasePr.getActividadByMateria(id).collect{ it ->
                    Log.d("Actividades", it.toString())
                    _actividades.value = it
                }
            }
            setLoading(false)
        } else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
        }
    }


    fun getActivities(id: Int, context: Context) {
        Log.i("id", id.toString())
        if (isOnline(context)) {
            setLoading(true)
            istOnline.postValue(true)
            viewModelScope.launch {
                useCasePr.getActividadByMateria(id).collect{ it ->
                    Log.d("Actividades", it.toString())
                    _actividades.value = it
                }
            }
            setLoading(false)
        } else {
            istOnline.postValue(false)
        }
    }
    fun getActivityById(id: Int) {
        viewModelScope.launch {
            useCasePr.getActiviyById(id).collect{ it ->
                _activityDetails.value = it
                Log.d("te traje esta actividad", it.toString())
            }
        }
    }

    fun getNotas(id_Actividad: Int, asignaturaS: AsignaturaDomain, context: Context) {
        if (isOnline(context)) {
            setLoading(true)
            val estudiantesMateria = asignaturaS.estudiantes

            viewModelScope.launch {
                useCasePr.getNotasToUpdate(id_Actividad, asignaturaS.id).collect { notas ->
                    val notasConNombreSet = mutableSetOf<NotasWithName>()

                    notas.forEach { nota ->
                        estudiantesMateria?.find { it.id == nota?.studentId }?.let { estudiante ->
                            val nombre = estudiante.nombre
                            val fechaNacimiento = estudiante.fecha
                            val sexo = estudiante.sexo

                            // Calcula la edad usando la función calcularEdad
                            val edad = calcularEdad("${fechaNacimiento}")

                            // Crear un objeto NotasWithName incluyendo la edad y el sexo
                            val notasWithName = NotasWithName(
                                id = nota!!.id,
                                studentId = nota!!.studentId,
                                asignaturaId = nota!!.asignaturaId,
                                actividadId = nota!!.actividadId,
                                puntuacion = nota!!.puntuacion,
                                fecha = nota!!.fecha,
                                nombre = nombre,
                                edad = edad,
                                sexo = sexo
                            )

                            // Añadir a la lista de notas con nombre
                            notasConNombreSet.add(notasWithName)
                        }
                    }

                    _notasConNombre.value = notasConNombreSet.toList() // Convertimos a lista para la asignación
                    Log.e("mutable_list", _notasConNombre.value.toString())
                    setLoading(false)
                }
            }
        } else {
            istOnline.postValue(false)
        }
    }


    fun getNotasStadisticas(asignaturaS: AsignaturaDomain, context: Context) {
        if (isOnline(context)) {
            setLoading(true)
            val estudiantesMateria = asignaturaS.estudiantes

            viewModelScope.launch {
                useCasePr.stadictsUc(asignaturaS.id).collect { notas ->
                    val notasConNombreSet = mutableSetOf<NotasEstadisticas>()

                    notas.forEach { nota ->
                        estudiantesMateria?.find { it.id == nota?.studentId }?.let { estudiante ->
                            val nombre = estudiante.nombre
                            val fechaNacimiento = estudiante.fecha
                            val sexo = estudiante.sexo

                            val edad = calcularEdad("${fechaNacimiento}")

                            // Crear un objeto NotasEstadisticas incluyendo la edad, sexo y puntuación
                            val notasEstadistica = NotasEstadisticas(
                                asignaturaId = nota!!.asignaturaId,
                                puntuacion = nota.puntuacion,
                                edad = edad,
                                sexo = sexo
                            )
                            // Agregar a notasConNombreSet
                            notasConNombreSet.add(notasEstadistica)
                        }
                    }

                    // Filtrar elementos con puntuación nula
                    val notasFiltradas = notasConNombreSet.filter { it.puntuacion != null }

                    // Asignar la lista filtrada a _notasStadisticas.value
                    _notasStadisticas.value = notasFiltradas.toList()

                    Log.e("mutable_list", _notasStadisticas.value.toString())
                    setLoading(false)
                }
            }
        } else {
            istOnline.postValue(false)
        }
    }


    fun calcularEdad(fechaNacimiento: String): Int {
        // Define el formato de fecha en formato "dd-MM-yyyy"
        val formato = DateTimeFormatter.ofPattern("dd-MM-yyyy")

        // Convierte la fecha de nacimiento de tipo String a LocalDate
        val fechaNacimientoDate = LocalDate.parse(fechaNacimiento, formato)

        // Obtiene la fecha actual
        val fechaActual = LocalDate.now()

        // Calcula la edad
        val edad = Period.between(fechaNacimientoDate, fechaActual).years
        Log.e("edad", edad.toString())
        return edad
    }


    fun editarNota(
        notas: NotasWithName,
        puntuacion: String,
        context: Context,
        asignaturaS: AsignaturaDomain?
    ) {
        val notaEdidata = NotaDomain(
            id = notas.id,
            actividadId = notas.actividadId,
            asignaturaId = notas.asignaturaId,
            studentId = notas.studentId,
            puntuacion = puntuacion.toInt(),
            fecha = notas.fecha
        )
        if (isOnline(context)) {
            setLoading(true)
            viewModelScope.launch {
                useCasePr.editarNota(notas.id, notaEdidata)
            }
            getNotas(notas.actividadId, asignaturaS!!, context)
            setLoading(false)
        } else istOnline.postValue(false)
    }

    fun filtrarAsignaturasByName() {
        val estudiantes = asignaturaDetails.value?.estudiantes
        estudiantes?.let { estudiantes ->
            val listaFiltrada = estudiantes.sortedBy { it.nombre }
            Log.d("lista filtrada por nombres", listaFiltrada.toString())
            _listaFiltrada.value = listaFiltrada
        }
    }

    fun filtrarAsignaturasByAge() {
        val estudiantes = asignaturaDetails.value?.estudiantes
        estudiantes?.let { estudiantes ->
            // Calcula la edad de cada estudiante y ordénalos por edad
            val listaFiltrada = estudiantes.map { estudiante ->
                // Calcula la edad usando la función `calcularEdad`
                val edad = calcularEdad(estudiante.fecha)

                // Crea una copia del estudiante con la edad calculada como una nueva propiedad
                estudiante.copy(edad = edad)
            }.sortedBy { it.edad }

            // Log para depuración
            Log.d("Lista filtrada por edades", listaFiltrada.toString())

            // Asigna la lista filtrada a `_listaFiltrada`
            _listaFiltrada.value = listaFiltrada
        }
    }


    fun filtrarAsignaturasByFecha() {
        val estudiantes = asignaturaDetails.value?.estudiantes
        estudiantes?.let { estudiantes ->
            val listaFiltrada = estudiantes.sortedBy { it.fechaIns }
            Log.d("lista filtrada por fechas", listaFiltrada.toString())
            _listaFiltrada.value = listaFiltrada
        }
    }

}