package com.example.eduapp.ui.compose.estudiantes.screens

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.eduapp.R
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.ui.compose.dataToView.EstadisticasPorEdad
import com.example.eduapp.ui.compose.dataToView.EstadisticasPorSexo
import com.example.eduapp.ui.compose.dataToView.NotasEstadisticas
import com.example.eduapp.ui.compose.estudiantes.ViewModelStudent
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose
import com.github.tehras.charts.bar.BarChart
import com.github.tehras.charts.bar.BarChartData
import com.github.tehras.charts.bar.renderer.label.SimpleValueDrawer


@Composable
fun EstadisticasEs(
    studentviewModel: ViewModelStudent,
    idAsignatura: Int?,
    navController: NavHostController
) {

    val context = LocalContext.current
    val asignaturaS by studentviewModel.asignaturaDetails.collectAsState()

    LaunchedEffect(idAsignatura) {
            if (idAsignatura != null) {
                studentviewModel.getAsignaturaById(idAsignatura)
                if (asignaturaS != null){
                    studentviewModel.getNotasStadisticas(asignaturaS!!, context)
                }
        }
    }

    val notasParaEstadistica by studentviewModel.notasStadisticas.collectAsState(initial = null)

    Log.e("notas estaadustucas", notasParaEstadistica.toString())


   val estadisticasEdad = notasParaEstadistica?.let { calcularEstadisticasPorEdadEs(it) }

    val estadisticasPorSexo = notasParaEstadistica?.let { calcularEstadisticasPorSexoEs(it) }
    val isLoading by studentviewModel.isLoadingFlow.collectAsState(initial = false)


    if (estadisticasEdad == null || estadisticasPorSexo == null) {
        ProgressBarCompose()
    } else {
        ContentEstadisticasEs(asignaturaS, estadisticasEdad, estadisticasPorSexo)
    }



}

@Composable
fun ContentEstadisticasEs(
    asignaturaS: AsignaturaDomain?,
    estadisticasEdad: List<EstadisticasPorEdad>,
    estadisticasPorSexo: List<EstadisticasPorSexo>
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
            .background(color = colorResource(id = R.color.white)),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {
            // Muestra el título
            Text(
                text = "Estadisticas de ${asignaturaS?.nombre}",
                style = TextStyle(fontSize = 32.sp),
                modifier = Modifier.padding(16.dp)
            )
        }

        // Muestra las barras de edad
        item {
            Text(
                text = "Estadisticas por la edad de las notas de ${asignaturaS?.nombre}",
                style = TextStyle(fontSize = 24.sp),
                modifier = Modifier.padding(16.dp)
            )
            BarrasEdadEs(estadisticasEdad)
        }

        // Muestra las barras de sexo
        item {
            Text(
                text = "Estadisticas por el sexo de las notas de ${asignaturaS?.nombre}",
                style = TextStyle(fontSize = 24.sp),
                modifier = Modifier.padding(16.dp)
            )
            BarrasSexoEs(estadisticasPorSexo)
        }
    }
}
fun calcularEstadisticasPorEdadEs(notas: List<NotasEstadisticas>): List<EstadisticasPorEdad> {
    // Agrupar las notas por edad
    val notasPorEdad = notas.groupBy { it.edad }

    // Calcular el promedio de puntuación para cada edad
    val estadisticasPorEdad = notasPorEdad.map { (edad, notasPorEdad) ->
        val promedio = notasPorEdad.mapNotNull { it.puntuacion }.average().toFloat()
        EstadisticasPorEdad(
            edad = edad,
            promedio = promedio
        )
    }

    return estadisticasPorEdad
}

fun calcularEstadisticasPorSexoEs(notas: List<NotasEstadisticas>): List<EstadisticasPorSexo> {
    // Agrupar las notas por sexo
    val notasPorSexo = notas.groupBy { it.sexo }

    // Calcular el promedio de puntuación para cada sexo
    val estadisticasPorSexo = notasPorSexo.map { (sexo, notasPorSexo) ->
        val promedio = notasPorSexo.mapNotNull { it.puntuacion }.average().toFloat()
        EstadisticasPorSexo(
            sexo = sexo,
            promedio = promedio
        )
    }

    return estadisticasPorSexo
}


@Composable
fun BarrasEdadEs(notas: List<EstadisticasPorEdad>){

    var barras = ArrayList<BarChartData.Bar>()
    notas.mapIndexed { index, notasEstadisticas ->
        barras.add(
            BarChartData.Bar(
                label = notasEstadisticas.edad.toString(),
                value = notasEstadisticas.promedio.toFloat(),
                color = colorResource(id = R.color.orange)
            )
        )
    }
    BarChart(barChartData = BarChartData(
        bars = barras),
        modifier = Modifier
            .padding(20.dp)
            .height(300.dp),
        labelDrawer = SimpleValueDrawer(
            drawLocation = SimpleValueDrawer.DrawLocation.XAxis
        )
    )
}

@Composable
fun BarrasSexoEs(notas: List<EstadisticasPorSexo>){

    var barras = ArrayList<BarChartData.Bar>()
    notas.mapIndexed { index, notasEstadisticas ->
        barras.add(
            BarChartData.Bar(
                label = notasEstadisticas.sexo,
                value = notasEstadisticas.promedio.toFloat(),
                color = randomColorEs()
            )
        )
    }
    BarChart(barChartData = BarChartData(
        bars = barras),
        modifier = Modifier
            .padding(20.dp)
            .height(300.dp),
        labelDrawer = SimpleValueDrawer(
            drawLocation = SimpleValueDrawer.DrawLocation.XAxis
        )
    )
}

// Variable global para la lista de colores
private val colorList = mutableListOf(Color(0xFFCE56C2), Color.Blue)

// Función para alternar entre los colores
private var currentColorIndex = 0

fun randomColorEs(): Color {
    // Alternar entre los colores
    val color = colorList[currentColorIndex]
    // Cambiar el índice para el próximo color
    currentColorIndex = (currentColorIndex + 1) % colorList.size
    // Retornar el color seleccionado
    return color
}
