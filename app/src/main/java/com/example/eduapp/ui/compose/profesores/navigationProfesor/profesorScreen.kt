package com.example.eduapp.ui.compose.profesores.navigationProfesor

sealed class ProfesorScreen (val routes: String) {
    object AsignaturaScreen: ProfesorScreen("AsignaturaScreen")
    object DetaillesAsignaturaScreen: ProfesorScreen("DetailsAsignaturaScreen")
    object  EstudiantesInscritosScreen: ProfesorScreen("EstudiantesInscritosScreen")
    object DetallesEstudiante: ProfesorScreen("detallesEstudiantes")
    object ActividadesScreen: ProfesorScreen("ActividadesScreen")
    object NotasScreen: ProfesorScreen("NotasScreen")
    object EstadisticaProfesor: ProfesorScreen("EstadisticasProfesor")
}