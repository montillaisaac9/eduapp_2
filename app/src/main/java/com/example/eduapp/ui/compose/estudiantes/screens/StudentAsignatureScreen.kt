package com.example.eduapp.ui.compose.estudiantes.screens

import android.content.Context
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.eduapp.R
import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.StudentDomain
import com.example.eduapp.ui.compose.estudiantes.ViewModelStudent
import com.example.eduapp.ui.compose.estudiantes.navigationStudent.StudentScreens
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter



@Composable
fun ScreenAsignaturasStudent(
    userInfoState: List<StudentDomain>,
    studentviewModel: ViewModelStudent,
    navController: NavHostController
) {


    var id_user by remember { mutableStateOf("") }
    var name_user by remember { mutableStateOf("") }
    var fecha_nacimiento by remember { mutableStateOf("") }
    var sexo_user by remember { mutableStateOf("") }

    var context = LocalContext.current
    var scope = rememberCoroutineScope()

    var asignaturasInscritas by remember { mutableStateOf<List<MateriaInscita>>(emptyList()) }


    val asignaturas by studentviewModel.asignaturas.collectAsState(initial = emptyList())

    LaunchedEffect(Unit) {
        studentviewModel.getAsignaturesForInscripcion(context)
        Log.d("la lista de asignaturas es:", asignaturas.toString())
    }

    LaunchedEffect(userInfoState) {
        userInfoState.forEach {
            id_user = it.id
            name_user = it.nombre
            fecha_nacimiento = it.fechaNacimiento
            asignaturasInscritas = it.materias
            sexo_user = it.sexo
        }
    }

    val isLoading by studentviewModel.isLoadingFlow.collectAsState(initial = false)

    if (isLoading) ProgressBarCompose()
    else asignaturasContentStudent(asignaturas, id_user, name_user, fecha_nacimiento, asignaturasInscritas, sexo_user, studentviewModel, context, scope, navController)


}

@OptIn(ExperimentalMaterialApi::class)
private @Composable
fun asignaturasContentStudent(
    asignaturas: List<AsignaturaDomain?>,
    id_user: String,
    name_user: String,
    fecha_nacimiento: String,
    asignaturasInscritas: List<MateriaInscita>,
    sexo_user: String,
    studentviewModel: ViewModelStudent,
    context: Context,
    scope: CoroutineScope,
    navController: NavHostController
) {

    val state = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        skipHalfExpanded = true
    )

    ModalBottomSheetLayout(
        sheetState = state,
        sheetContent = {
            Text(
                text = "Asignaturas para Inscribir",
                style = TextStyle(fontSize = 28.sp),
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(16.dp, top = 40.dp)
            )
            LazyColumn(modifier = Modifier.height(500.dp)) {
                asignaturas.let { asignaturas ->
                    items(asignaturas.size) { index ->
                        val asignatura = asignaturas[index]
                        asignatura?.let {
                            ListItemRowMaterias(
                                it,
                                studentviewModel,
                                id_user,
                                name_user,
                                fecha_nacimiento,
                                asignaturasInscritas,
                                context,
                                sexo_user
                            )
                        }
                    }
                }
            }
        },
        sheetShape = RoundedCornerShape(topStart = 30.dp, topEnd = 30.dp)
    ) {
        Column(Modifier.fillMaxSize()) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .background(color = colorResource(id = R.color.white)),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Asignaturas Inscritas",
                    style = TextStyle(fontSize = 32.sp),
                    modifier = Modifier.padding(16.dp)
                )
                IconButton(
                    onClick = { scope.launch {
                        state.show()
                    }},
                    modifier = Modifier.background(color = colorResource(id = R.color.orange), shape = CircleShape)
                ) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = "inscripcion",
                        tint = Color.White,
                        modifier = Modifier.size(40.dp)
                    )

            }
            }
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                asignaturasInscritas.let { asignaturas ->
                    items(asignaturas.size) { index ->
                        val asignatura = asignaturas[index]
                        ListItemRowMateriasInscritas(asignatura, navController, studentviewModel)
                    }
                }
            }

        }
    }
}


private @Composable
fun ListItemRowMaterias(
    it: AsignaturaDomain,
    studentVm: ViewModelStudent,
    id_user: String,
    name_user: String,
    fecha_user: String,
    asignaturasInscritas: List<MateriaInscita>,
    context: Context,
    sexo_user: String
) {

    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val dateFormat: LocalDateTime = LocalDateTime.now()

    Card(
        elevation = 6.dp,
        shape = RoundedCornerShape(8.dp),
        backgroundColor = colorResource(id = R.color.gray),
        modifier = Modifier
            .padding(20.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Text(
                text = it.nombre,
                textAlign = TextAlign.Center,
                style = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold),
                modifier = Modifier.fillMaxWidth()
            )
            Text(
                text = "Descripcion: ${it.descripcion}",
                textAlign = TextAlign.Start,
                style = TextStyle(fontSize = 14.sp),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp)
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp)
            ) {
                Button(
                    colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange)),
                    onClick = {
                        studentVm.inscripcionVm(
                            id_user,
                            name_user,
                            fecha_user,
                            it,
                            asignaturasInscritas,
                            context,
                            dateFormat,
                            sexo_user
                        )
                    },
                    modifier = Modifier
                        .align(Alignment.Center)
                        .height(32.dp)
                ) {
                    Text(text = "Inscribirse")
                }
            }
        }
    }
}


@Composable
fun ListItemRowMateriasInscritas(
    it: MateriaInscita,
    navController: NavHostController,
    studentviewModel: ViewModelStudent
) {
    Card(
        elevation = 6.dp,
        shape = RoundedCornerShape(8.dp),
        backgroundColor = colorResource(id = R.color.gray),
        modifier = Modifier
            .padding(20.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = it.nombre,
                textAlign = TextAlign.Center,
                style = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold),
                modifier = Modifier.fillMaxWidth()
            )
            Text(
                text = "Descripcion: ${it.descripcion}",
                textAlign = TextAlign.Start,
                style = TextStyle(fontSize = 14.sp),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp)
            )

                Button(
                    colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange)),
                    onClick = {
                        navController.navigate("${StudentScreens.studentActividadScreen.routes}/${it.id}")
                    },
                    modifier = Modifier
                        .padding(10.dp)
                        .height(32.dp)
                ) {
                    Text(text = "Ver Actividades")
                }
                Button(
                    colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange)),
                    onClick = {
                        studentviewModel.getAsignaturaById(it.id)
                        navController.navigate("${StudentScreens.studentEstadisticasScreen.routes}/${it.id}")
                    },
                    modifier = Modifier
                        .padding(10.dp)
                        .height(32.dp)
                ) {
                    Text(text = "Ver Estadisticas")
            }
        }
    }
}

