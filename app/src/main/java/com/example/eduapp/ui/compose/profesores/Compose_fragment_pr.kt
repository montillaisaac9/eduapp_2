

package com.example.eduapp.ui.compose.profesores

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.eduapp.R
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.databinding.FragmentComposeFragmentPrBinding
import com.example.eduapp.ui.compose.profesores.navigationProfesor.ProfesorNavigation
import com.example.eduapp.ui.compose.untils_ui.MaterialButton
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@AndroidEntryPoint
class Compose_fragment_pr: Fragment() {

    private var _binding: FragmentComposeFragmentPrBinding? = null
    private val binding get() = _binding!!

    private val profesorviewModel: ViewModelProfesor by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentComposeFragmentPrBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(R.id.compose_fragment_pr)
        }

        val compose = binding.ComposeContainer

        compose.setContent {
            viewContainer()
        }

        profesorviewModel.istOnline.observe(viewLifecycleOwner) { success ->
            if (!success) Toast.makeText(this.context, "No tiene conexion a internet para actualizar los datos", Toast.LENGTH_SHORT)
                .show()
        }

    }


    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    fun viewContainer() {

        LaunchedEffect(Unit) {
            profesorviewModel.getUser()
        }


        val context = LocalContext.current

        val userInfoState by profesorviewModel.user.collectAsState(initial = emptyList())


        var scaffoldState = rememberScaffoldState()
        var scope = rememberCoroutineScope()
        var name2 by remember { mutableStateOf("") }

        val isLoading by profesorviewModel.isLoadingFlow.collectAsState(initial = false)

        Scaffold(
            scaffoldState = scaffoldState,
            topBar = {
                if (isLoading) Column (modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp)
                    .background(colorResource(id = R.color.white))
                ){

                }
                else CustomTopAppBar(scope, scaffoldState, profesorviewModel) },
            drawerContent = { Drawer(userInfoState) },
            content = { ProfesorNavigation(userInfoState, profesorviewModel) }
        )
    }

    @Composable
    private fun Drawer(
        userInfoState: List<UserProfesorEntity>
    ) {


        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Perfil",
                    style = TextStyle(fontSize = 24.sp)
                )
            }


            Box(
                modifier = Modifier
                    .size(180.dp)
                    .padding(16.dp),
                contentAlignment = Alignment.Center,

                ) {
                Image(
                    painter = painterResource(id = R.drawable.teacher_icon),
                    contentDescription = "Icono de profesor",
                    modifier = Modifier.fillMaxSize()
                )
            }

            userInfoState.forEach { user ->
                Text(
                    text = "Nombre: ${user.nombre}",
                    style = TextStyle(fontSize = 18.sp),
                    modifier = Modifier.padding(16.dp)
                )

                Text(
                    text = "Correo: ${user.correo}",
                    style = TextStyle(fontSize = 18.sp),
                    modifier = Modifier.padding(16.dp)
                )

                Text(
                    text = "Teléfono: ${user.telefono}",
                    style = TextStyle(fontSize = 18.sp),
                    modifier = Modifier.padding(16.dp)
                )
            }
        }
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(16.dp),
            verticalArrangement = Arrangement.Bottom
        ) {
            MaterialButton(text = "Editar") {
                findNavController().navigate(R.id.action_compose_fragment_pr_to_editarPerfilFragmentPr)
            }
        }
    }


    @Composable
    fun CustomTopAppBar(
        scope: CoroutineScope,
        scaffoldState: ScaffoldState,
        profesorviewModel: ViewModelProfesor,
    ) {

        var showDialog by remember { mutableStateOf(false) }

        var shouldNavigate by remember { mutableStateOf(false) }

        LaunchedEffect(shouldNavigate) {
            if (shouldNavigate) {
                delay(1000)
                profesorviewModel.logout()
                findNavController().navigate(R.id.action_compose_fragment_pr_to_mainFragment)
            }
        }

        if (showDialog) {
            AlertDialog(
                onDismissRequest = { showDialog = false }, // Desactiva el diálogo si se cancela
                title = {
                    Text(text = "Confirmación de cierre de sesión")
                },
                text = {
                    Text(text = "¿Desea cerrar sesión?")
                },
                confirmButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colorResource(id = R.color.orange) // Configura el color de fondo del botón de confirmación
                        ),
                        onClick = {
                            profesorviewModel.logout()
                            shouldNavigate = true
                            showDialog = false
                        }
                    ) {
                        Text("Sí")
                    }
                },
                dismissButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colorResource(id = R.color.orange) // Configura el color de fondo del botón de confirmación
                        ),
                        onClick = {
                            showDialog = false
                        }
                    ) {
                        Text("No")
                    }
                }
            )
        }

        TopAppBar(
            title = {
                Text(
                    text = "Eduapp",
                    color = colorResource(id = R.color.white),
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            },
            navigationIcon = {
                IconButton(onClick = {
                    scope.launch {
                        scaffoldState.drawerState.open()
                    }
                }) {
                    Icon(
                        imageVector = Icons.Filled.AccountCircle,
                        contentDescription = "Icono de menu",
                        tint = colorResource(id = R.color.white)
                    )
                }
            },
            actions = {
                IconButton(onClick = {
                    showDialog = true
                }) {
                    Icon(
                        imageVector = Icons.Filled.ExitToApp,
                        contentDescription = "Buscar",
                        tint = colorResource(id = R.color.white)
                    )
                }
            },
            backgroundColor = colorResource(id = R.color.orange)
        )

    }

}


