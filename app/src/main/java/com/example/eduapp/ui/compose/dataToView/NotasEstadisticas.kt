package com.example.eduapp.ui.compose.dataToView

data class NotasEstadisticas(
    val asignaturaId: Int,
    val puntuacion: Int?,
    val edad: Int,
    val sexo: String
)

data class EstadisticasPorEdad(
    val edad: Int,
    val promedio: Float
)

data class EstadisticasPorSexo(
    val sexo: String,
    val promedio: Float
)