package com.example.eduapp.ui.compose.profesores.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.eduapp.R
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.profesores.navigationProfesor.ProfesorScreen
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose


@Composable
fun StudiantesInscritos(
    profesorviewModel: ViewModelProfesor,
    id: Int?,
    navController: NavHostController
) {

    val isLoading by profesorviewModel.isLoadingFlow.collectAsState(initial = false)
    val asignaturaS by profesorviewModel.asignaturaDetails.collectAsState(initial = null)
    val listaEstudent by profesorviewModel.listaFiltrada.collectAsState(initial = emptyList())
    profesorviewModel.filtrarAsignaturasByName()


    if (isLoading) {
        ProgressBarCompose()
    } else {
        contenido(profesorviewModel, id, navController, asignaturaS, listaEstudent)
    }

}

@Composable
fun contenido(
    profesorviewModel: ViewModelProfesor,
    id: Int?,
    navController: NavHostController,
    asignaturaS: AsignaturaDomain?,
    listaEstudent: List<EstudianteInscrito>
) {

    var expanded by remember { mutableStateOf(false) }
    var orderByAlphabetical by remember { mutableStateOf(true) }
    var orderByAge by remember { mutableStateOf(false) }
    var orderByRegistrationDate by remember { mutableStateOf(false) }

    val context = LocalContext.current


    LaunchedEffect(orderByAlphabetical) {
        if (orderByAlphabetical) {
            profesorviewModel.filtrarAsignaturasByName()
        }
    }

    LaunchedEffect(orderByAge) {
        if (orderByAge) {
            profesorviewModel.filtrarAsignaturasByAge()
        }
    }

    LaunchedEffect(orderByRegistrationDate) {
        if (orderByRegistrationDate) {
            profesorviewModel.filtrarAsignaturasByFecha()
        }
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = colorResource(id = R.color.white)),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Filtros:",
                style = TextStyle(fontSize = 32.sp),
                modifier = Modifier.padding(16.dp)
            )

            IconButton(
                onClick = { expanded = !expanded },
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .background(
                        color = colorResource(id = R.color.orange),
                        shape = RoundedCornerShape(20.dp)
                    )
            ) {
                Icon(
                    imageVector = if (expanded) Icons.Default.KeyboardArrowUp else Icons.Default.KeyboardArrowDown,
                    contentDescription = "Expandir/Cerrar menú",
                    tint = Color.White
                )
            }
        }
        if (expanded) {
            TextSwitchRow(
                text = "Orden Alfabético:",
                checked = orderByAlphabetical,
                onCheckedChange = {
                    if (!it) {
                        if (!orderByAge && !orderByRegistrationDate) {
                            showToast("No puedes apagar todos los filtros", context)
                        } else {
                            orderByAlphabetical = it
                        }
                    } else {
                        orderByAge = false
                        orderByRegistrationDate = false
                        orderByAlphabetical = it
                    }
                }
            )

            TextSwitchRow(
                text = "Orden de Edad de Mayor a Menor:",
                checked = orderByAge,
                onCheckedChange = {
                    if (!it) {
                        if (!orderByAlphabetical && !orderByRegistrationDate) {
                            showToast("No puedes desactivar todos los filtros", context)
                        } else {
                            orderByAge = it
                        }
                    } else {
                        orderByAlphabetical = false
                        orderByRegistrationDate = false
                        orderByAge = it
                    }
                }
            )
            TextSwitchRow(
                text = "Fecha de Inscripción:",
                checked = orderByRegistrationDate,
                onCheckedChange = {
                    if (!it) {
                        if (!orderByAlphabetical && !orderByAge) {
                            showToast("No puedes apagar todos los filtros", context)
                        } else {
                            orderByRegistrationDate = it
                        }
                    } else {
                        orderByAlphabetical = false
                        orderByAge = false
                        orderByRegistrationDate = it
                    }
                }
            )
        }

        asignaturaS?.let {
            Text(
                text = it.nombre,
                style = TextStyle(fontSize = 32.sp),
                modifier = Modifier.padding(16.dp)
            )
        }

        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(listaEstudent.size) { index ->
                val asignatura = listaEstudent[index]
                ListItemRow(asignatura, index, asignaturaS, context, navController, profesorviewModel, )
            }
        }
    }
}


@Composable
fun ListItemRow(it: EstudianteInscrito, index: Int, asignaturaDomain: AsignaturaDomain?, context: Context, navController: NavHostController, profesorVm: ViewModelProfesor) {
    var counter = index+1

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        Box(
            modifier = Modifier
                .weight(1f)
                .background(
                    color = colorResource(id = R.color.orange),
                    shape = CircleShape
                ),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = counter.toString(),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                style = TextStyle(fontSize = 24.sp)
            )
        }

        Spacer(modifier = Modifier.width(10.dp))

        Box(
            modifier = Modifier
                .weight(4f)
                .background(
                    color = colorResource(id = R.color.orange),
                    shape = RoundedCornerShape(8.dp)
                )
                .clickable { navController.navigate("${ProfesorScreen.DetallesEstudiante.routes}/${it.id}") },
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = it.nombre,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                style = TextStyle(fontSize = 24.sp)
            )
        }

        Spacer(modifier = Modifier.width(10.dp))

        IconButton(
            onClick = {
                profesorVm.delete(it, asignaturaDomain, context)
                      },
            modifier = Modifier
                .size(40.dp)
                .background(
                    color = colorResource(id = R.color.delete),
                    shape = CircleShape
                )
                .align(Alignment.CenterVertically)
        ) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = "Eliminar",
                tint = Color.White
            )
        }
    }

}

@Composable
fun TextSwitchRow(
    text: String,
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit
) {
    val switchColors = SwitchDefaults.colors(
        checkedThumbColor = Color(0xFFE88121), // Color naranja cuando está encendido
        uncheckedThumbColor = Color(0xFFC06007), // Color naranja oscuro cuando está apagado
        checkedTrackColor = Color(0xFFE88121).copy(alpha = 0.5f), // Color del track cuando está encendido
        uncheckedTrackColor = Color(0xFFC06007).copy(alpha = 0.5f) // Color del track cuando está apagado
    )

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = text,
            style = TextStyle(fontSize = 15.sp),
            modifier = Modifier.padding(16.dp)
        )
        Switch(
            checked = checked,
            onCheckedChange = onCheckedChange,
            modifier = Modifier.padding(end = 30.dp),
            colors = switchColors
        )
    }
}

fun showToast(message: String, context: Context) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}