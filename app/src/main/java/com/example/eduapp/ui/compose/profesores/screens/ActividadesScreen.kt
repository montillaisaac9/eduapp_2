package com.example.eduapp.ui.compose.profesores.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDefaults.colors
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.eduapp.R
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.profesores.navigationProfesor.ProfesorScreen
import com.example.eduapp.ui.compose.untils_ui.CustomButton
import com.example.eduapp.ui.compose.untils_ui.MaterialButton
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


@OptIn(ExperimentalMaterialApi::class, ExperimentalMaterial3Api::class)
@Composable
fun ActividadesScreen(
    profesorviewModel: ViewModelProfesor,
    id_asignatura: Int?,
    navController: NavController,
) {

    val asinature_id = id_asignatura

    val context = LocalContext.current

    LaunchedEffect(id_asignatura) {
        if (id_asignatura != 0) {
            id_asignatura?.let { profesorviewModel.getActivities(it, context) }
        }
    }

    val orangeDark = Color(0xFFC06007)
    val orange = Color(0xFFE88121)

    val orangeDatePickerColors = colors(
        selectedDayContentColor = orangeDark,
        selectedDayContainerColor = orange,
        disabledSelectedDayContainerColor = orange,
        todayContentColor = orange,
        todayDateBorderColor = orangeDark,
        dividerColor = orange
    )

    var scope = rememberCoroutineScope()

    var id by remember { mutableStateOf(0) }



    val actividades by profesorviewModel.actividades.collectAsState(initial = emptyList())


    var name by remember { mutableStateOf("") }
    var description by remember { mutableStateOf("") }
    var maxPoints by remember { mutableStateOf("") }
    var fecha by remember { mutableStateOf("") }

    val state = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        skipHalfExpanded = true
    )

    val dayPickerState = rememberDatePickerState()
    var showDialog by remember { mutableStateOf(false) }



    ModalBottomSheetLayout(
        sheetState = state,

        sheetContent = {
            LazyColumn(modifier = Modifier.size(600.dp)) {
                item {                 Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(50.dp)
                        .background(
                            color = colorResource(id = R.color.white),
                            shape = RoundedCornerShape(20.dp)
                        ),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    CustomButton(
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(end = 16.dp),
                        onClick = {
                            name = ""
                            description = ""
                            scope.launch {
                                state.hide()
                            }
                        },
                        icon = Icons.Default.ArrowBack // Aquí pasas el icono que desees
                    )
                    Text(
                        text = "Crea tu actividad",
                        style = TextStyle(fontSize = 32.sp),
                        modifier = Modifier.padding(16.dp)
                    )
                }
                    Text(
                        text = "nombre de la Actividad:",
                        style = TextStyle(fontSize = 18.sp),
                        modifier = Modifier
                            .padding(start = 25.dp)
                            .fillMaxWidth()
                    )
                    TextField(
                        value = name, onValueChange = { name = it }, modifier = Modifier
                            .padding(start = 25.dp, end = 25.dp)
                            .fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true,
                        colors = TextFieldDefaults.textFieldColors(
                            cursorColor = colorResource(id = R.color.orange), // Color del cursor
                            focusedIndicatorColor = colorResource(id = R.color.orange), // Color de la línea de base cuando el TextField está enfocado
                            unfocusedIndicatorColor = colorResource(id = R.color.gray) // Color de la línea de base cuando el TextField no está enfocado
                        )
                    )
                    Text(
                        text = "descripcion de la Actividad:",
                        style = TextStyle(fontSize = 18.sp),
                        modifier = Modifier
                            .padding(start = 25.dp, top = 30.dp)
                            .fillMaxWidth()
                    )
                    TextField(
                        value = description,
                        onValueChange = { description = it },
                        modifier = Modifier
                            .padding(start = 25.dp, end = 25.dp)
                            .fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true,
                        colors = TextFieldDefaults.textFieldColors(
                            cursorColor = colorResource(id = R.color.orange), // Color del cursor
                            focusedIndicatorColor = colorResource(id = R.color.orange), // Color de la línea de base cuando el TextField está enfocado
                            unfocusedIndicatorColor = colorResource(id = R.color.gray) // Color de la línea de base cuando el TextField no está enfocado
                        )
                    )
                    Text(
                        text = "Puntuacion Maxima",
                        style = TextStyle(fontSize = 18.sp),
                        modifier = Modifier
                            .padding(start = 25.dp, top = 30.dp)
                            .fillMaxWidth()
                    )
                    TextField(
                        value = maxPoints,
                        onValueChange = { maxPoints = it },
                        modifier = Modifier
                            .padding(start = 25.dp, end = 25.dp)
                            .fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true,
                        colors = TextFieldDefaults.textFieldColors(
                            cursorColor = colorResource(id = R.color.orange), // Color del cursor
                            focusedIndicatorColor = colorResource(id = R.color.orange), // Color de la línea de base cuando el TextField está enfocado
                            unfocusedIndicatorColor = colorResource(id = R.color.gray) // Color de la línea de base cuando el TextField no está enfocado
                        )
                    )
                    if (showDialog) {
                        val fechaActual = Date()
                        val minDate = fechaActual
                        val maxDate = Calendar.getInstance().apply { add(Calendar.YEAR, 1) }.time

                        var isDateInRange = false

                        DatePickerDialog(
                            onDismissRequest = { showDialog = false },
                            confirmButton = {
                                Button(
                                    onClick = {
                                        val selectedDateMillis = dayPickerState.selectedDateMillis
                                        if (selectedDateMillis != null) {
                                            val selectedDate = Date(selectedDateMillis)
                                            isDateInRange = selectedDate.after(minDate) && selectedDate.before(maxDate)
                                        }
                                        if (isDateInRange) {
                                            val selectedDate = dayPickerState.selectedDateMillis?.let { Date(it) }
                                            val formatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
                                            val formattedDate = formatter.format(selectedDate)
                                            fecha = formattedDate
                                            showDialog = false
                                        } else {
                                            Toast.makeText(context, "la fecha tiene q estar en el rango de hoy a 1 año", Toast.LENGTH_SHORT).show()
                                            fecha = ""
                                        }
                                    },
                                    colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange))
                                ) {
                                    Text(text = "Confirmar ")
                                }
                            },
                            dismissButton = {
                                Button(
                                    onClick = { showDialog = false},
                                    colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange))
                                ) {
                                    Text(text = "Cerrar ")
                                }
                            }
                        ) {
                            DatePicker(state = dayPickerState, colors = orangeDatePickerColors)
                        }
                    }

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(25.dp)
                            .height(75.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(
                            text = "Fecha seleccionada: $fecha",
                            modifier = Modifier.padding(start = 10.dp),
                            style = TextStyle(fontSize = 15.sp)
                        )
                        IconButton(
                            onClick = { showDialog = true },
                            modifier = Modifier.background(color = colorResource(id = R.color.orange), shape = CircleShape)
                        ) {
                            Icon(
                                imageVector = Icons.Default.DateRange,
                                contentDescription = "Fecha",
                                tint = Color.White,
                                modifier = Modifier.size(40.dp)
                            )
                        }



                    }


                    MaterialButton(text = "Guardar") {

                        if (description.isEmpty() or name.isEmpty() or maxPoints.isEmpty() or fecha.isEmpty() )Toast.makeText(context, "rellene los campos", Toast.LENGTH_SHORT).show()
                        else {
                            asinature_id?.let {
                                guardarActividad(name, description, maxPoints, fecha,
                                    it, context, profesorviewModel)
                            }
                            name = ""
                            description = ""
                            maxPoints = ""
                            fecha = ""
                            scope.launch {
                                state.hide()
                            }
                        }
                    } }
            }
        },
        sheetShape = RoundedCornerShape(topStart = 30.dp, topEnd = 30.dp)
    ) {
        Column(Modifier.fillMaxSize()) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .background(color = colorResource(id = R.color.white)),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Asignaturas",
                    style = TextStyle(fontSize = 32.sp),
                    modifier = Modifier.padding(16.dp)
                )
                CustomButton(
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(end = 16.dp),
                    onClick = {
                        scope.launch {
                            state.show()
                        }
                    },
                    icon = Icons.Default.Add // Aquí pasas el icono que desees
                )
            }
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                items(actividades.size) { index ->
                    val actividades = actividades[index]
                    actividades?.let { ListActividadesRow(it, context, navController, asinature_id) }
                }
            }
        }
    }
}

fun guardarActividad(name: String, description: String, maxPoints: String, fecha: String, id: Int, context: Context, profesorviewModel: ViewModelProfesor) {
    profesorviewModel.crearActivity(name, description, maxPoints, fecha, id, context)
}


@Composable
fun ListActividadesRow(
    actividad: ActividadDomain,
    context: Context,
    navController: NavController,
    asinature_id: Int?
) {
    val coroutineScope = rememberCoroutineScope()
    var clickableEnabled by remember { mutableStateOf(true) }
    var expanded by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 20.dp)
            .background(
                color = colorResource(id = R.color.orange),
                shape = RoundedCornerShape(8.dp) // Agregar la forma redondeada
            )
            .clickable {
                if (clickableEnabled) {
                    clickableEnabled = false // Desactivar el clic
                    expanded = !expanded
                    coroutineScope.launch {
                        Toast
                            .makeText(context, actividad.nombre, Toast.LENGTH_SHORT)
                            .show()
                        delay(1500L)
                        clickableEnabled = true
                    }
                }
            }
            .animateContentSize()
    ) {
        if (expanded) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Column (
                    modifier = Modifier
                        .padding(10.dp)
                ) {
                    Text(
                        text = actividad.nombre,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .padding(vertical = 16.dp)
                            .fillMaxWidth(),
                        style = TextStyle(fontSize = 24.sp)
                    )
                    Text(
                        text = actividad.descripcion,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .padding(vertical = 16.dp)
                            .fillMaxWidth(),
                        style = TextStyle(fontSize = 15.sp)
                    )
                Row (
                    modifier = Modifier.fillMaxWidth(),
                    Arrangement.SpaceBetween
                ){
                    Text(
                        text = "puntuacion Maxima: ${actividad.puntuacion_Max.toString()}",
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .padding(vertical = 16.dp),
                        style = TextStyle(fontSize = 15.sp)
                    )
                    Text(
                        text = "fecha de Limite: ${actividad.fecha_limit}",
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .padding(vertical = 16.dp),
                        style = TextStyle(fontSize = 15.sp)
                    )
                }
                    Row (
                        modifier = Modifier.fillMaxWidth(),
                        Arrangement.Center
                    ){
                        Button(
                            colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.gray)),
                            onClick = {
                                navController.navigate("${ProfesorScreen.NotasScreen.routes}/$asinature_id/${actividad.id}")
                            },
                            modifier = Modifier
                                .height(32.dp)
                        ) {
                            Text(text = "Calificaciones")
                        }
                    }
                }
            }
        } else {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = actividad.nombre,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(vertical = 16.dp)
                        .fillMaxWidth(),
                    style = TextStyle(fontSize = 24.sp)
                )
            }
        }
    }
}
