package com.example.eduapp.ui.compose.untils_ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.eduapp.R
import com.google.android.material.progressindicator.CircularProgressIndicator

@Composable
fun MaterialButton(text: String, onClickAction: () -> Unit) {
    Surface(
        onClick = onClickAction,
        shape = CircleShape,
        color = colorResource(id = R.color.orange),
        modifier = Modifier.padding(20.dp).fillMaxWidth(),
    ) {
        Text(
            text = text,
            fontSize = 20.sp,
            color = Color.White,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(20.dp)
        )
    }
}

@Composable
fun CustomButton(
    modifier: Modifier,
    onClick: () -> Unit,
    icon: ImageVector // Agregamos un parámetro para el icono
) {
    Box(
        modifier = modifier
            .clickable { onClick() } // Añade el listener de clic
            .size(width = 49.dp, height = 46.dp)
            .background(
                color = colorResource(id = R.color.orange),
                shape = RoundedCornerShape(20.dp)
            ),
        contentAlignment = Alignment.Center
    ) {
        Icon(
            imageVector = icon, // Utiliza el icono pasado como parámetro
            contentDescription = null,
            tint = Color.White
        )
    }
}



@Composable
fun ProgressBarCompose() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White), // Color de fondo blanco para el contenedor
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.width(40.dp),
            color = colorResource(id = R.color.orange_dark),
            strokeCap = StrokeCap.Round,
            backgroundColor = colorResource(id = R.color.orange)
        )
    }
}





