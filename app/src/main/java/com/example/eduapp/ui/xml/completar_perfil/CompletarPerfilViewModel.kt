package com.example.eduapp.ui.xml.completar_perfil

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eduapp.domain.auth.UseCaseAuth
import com.example.eduapp.domain.model.NewProfesorDomain
import com.example.eduapp.domain.model.NewStudentDomain
import com.example.eduapp.untils.isOnline
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CompletarPerfilViewModel @Inject constructor(
    private val useCaseAuth: UseCaseAuth
) : ViewModel() {

    val userLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val istOnline: MutableLiveData<Boolean> = MutableLiveData()
    val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val errorLiveData: MutableLiveData<String> = MutableLiveData()


    fun registrarPr(
        email: String,
        password: String,
        nombre: String,
        telefono: String,
        rol: String,
        requireContext: Context
    ) {
        isLoadingLiveData.postValue(true)
        val newProfesorDomain = NewProfesorDomain(
            correo = email,
            contraseña = password,
            nombre = nombre,
            telefono = telefono
        )

        if (isOnline(requireContext)) {
            istOnline.postValue(false)
            guardarPr(newProfesorDomain)
        } else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
            isLoadingLiveData.postValue(false)
        }
    }

    fun registrarEs(
        nombre: String,
        email: String,
        password: String,
        fecha: String,
        base64String: String,
        sexoSeleccionado: String?,
        telefono: String,
        latitude: String?,
        longitude: String?,
        rol: String?,
        requireContext: Context
    ) {
        isLoadingLiveData.postValue(true)
        val newStudentDomain = NewStudentDomain(
            nombre = nombre,
            correo = email,
            contraseña = password,
            fechaNacimiento = fecha,
            foto = base64String,
            sexo = sexoSeleccionado,
            telefono = telefono,
            latitud = latitude,
            longitud = longitude,
            materias = emptyList()
        )

        if (isOnline(requireContext)) {
            istOnline.postValue(false)
            guardarEs(newStudentDomain)
        } else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
            isLoadingLiveData.postValue(false)
        }
    }


    private fun guardarEs(newStudentDomain: NewStudentDomain) {
        viewModelScope.launch {
            val result = useCaseAuth.registerS(newStudentDomain)
            Log.i("inicio", result.toString())
            if (result) userLiveData.postValue(true)
            else userLiveData.postValue(false)

        }
    }

    private fun guardarPr(newProfesorDomain: NewProfesorDomain) {
        viewModelScope.launch {
            val result = useCaseAuth.registerP(newProfesorDomain)
            Log.i("inicio", "logrado")
            if (result) userLiveData.postValue(true)
            else userLiveData.postValue(false)
        }
    }
}