package com.example.eduapp.ui.compose.profesores.navigationProfesor

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.profesores.screens.ActividadesScreen
import com.example.eduapp.ui.compose.profesores.screens.AsignaturasScreen
import com.example.eduapp.ui.compose.profesores.screens.CalificacionesScreenProfesor
import com.example.eduapp.ui.compose.profesores.screens.DetallesEstudiante

import com.example.eduapp.ui.compose.profesores.screens.StudiantesInscritos
import com.example.eduapp.ui.compose.profesores.screens.detallesAsignaturas
import com.example.eduapp.ui.compose.untils_ui.Estadisticas

@Composable
fun ProfesorNavigation(
    userInfoState: List<UserProfesorEntity>,
    profesorviewModel: ViewModelProfesor,
) {

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = ProfesorScreen.AsignaturaScreen.routes){
        composable(route = ProfesorScreen.AsignaturaScreen.routes){
            AsignaturasScreen(userInfoState, profesorviewModel, navController)
        }
        composable(
            route = "${ProfesorScreen.DetaillesAsignaturaScreen.routes}/{idAsignatura}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType })
        ) {
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            detallesAsignaturas(profesorviewModel, idAsignatura, navController)
        }
        composable(
            route = "${ProfesorScreen.EstudiantesInscritosScreen.routes}/{idAsignatura}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType })
        ) {
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            StudiantesInscritos(profesorviewModel, idAsignatura, navController)
        }
        composable(
            route = "${ProfesorScreen.DetallesEstudiante.routes}/{idEstudiante}",
            arguments = listOf(navArgument("idEstudiante") { type = NavType.StringType })
        ) {
            val idEstudiante = it.arguments?.getString("idEstudiante")?.toString()
            DetallesEstudiante(profesorviewModel, idEstudiante, navController)
        }
        composable(
            route = "${ProfesorScreen.ActividadesScreen.routes}/{idAsignatura}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType })
        ) {
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            ActividadesScreen(profesorviewModel, idAsignatura, navController)
        }
        composable(
            route = "${ProfesorScreen.NotasScreen.routes}/{idAsignatura}/{idActividad}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType },navArgument("idActividad"){ type = NavType.StringType })
        ) {
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            val idActividad = it.arguments?.getString("idActividad")?.toIntOrNull()
            CalificacionesScreenProfesor(profesorviewModel, idAsignatura, idActividad, navController)
        }
        composable(
            route = "${ProfesorScreen.EstadisticaProfesor.routes}/{idAsignatura}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType })
        ){
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            Estadisticas(profesorviewModel, idAsignatura, navController)
        }
    }
}

