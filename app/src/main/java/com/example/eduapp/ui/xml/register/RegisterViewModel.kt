package com.example.eduapp.ui.xml.register


import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eduapp.domain.auth.UseCaseAuth

import com.example.eduapp.untils.isOnline
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val useCaseAuth: UseCaseAuth
) : ViewModel() {

    val userLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val istOnline: MutableLiveData<Boolean> = MutableLiveData()
    val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val errorLiveData: MutableLiveData<String> = MutableLiveData()


    fun verifiyUser(email: String, pass1: String, pass2: String, rol: Boolean, requireContext: Context) {
        val emailValid = verifyEmail(email)
        if (emailValid){
            if (isOnline(requireContext)) {
                istOnline.postValue(false)
                isLoadingLiveData.postValue(true)

                viewModelScope.launch {
                    val isP = useCaseAuth.verifyP(email)
                    val isE = useCaseAuth.verifyS(email)
                    if (isP == true) {
                        errorLiveData.postValue("Ya está registrado como Profesor")
                        isLoadingLiveData.postValue(false)
                    } else if (isE == true) {
                        errorLiveData.postValue("Ya está registrado como Estudiante")
                        isLoadingLiveData.postValue(false)
                    } else {
                        val isValidPassword = validarContrasena(pass1)
                        if (!isValidPassword) {
                            errorLiveData.postValue("La contraseña debe tener al menos 8 caracteres, con al menos una mayúscula, una minúscula, un símbolo y un número.")
                            isLoadingLiveData.postValue(false)
                        } else {
                            if (pass1 == pass2){
                                userLiveData.postValue(true)
                                isLoadingLiveData.postValue(false)
                            } else errorLiveData.postValue("La contraseñas deben coincidir.")
                        }
                    }
                }
            } else {
                Log.e("no", "no tengo internet boludo")
                istOnline.postValue(true)
            }
        } else {
            errorLiveData.postValue("email invalido")
        }
    }

    private fun verifyEmail(email: String): Boolean {
        val emailPattern = Regex("^\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(\\.\\w{2,})+\$")
        return if (emailPattern.matches(email)) true
        else false
    }
    private fun validarContrasena(contrasena: String): Boolean {
            val regex = Regex("^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[!@#\$%^&*()-+]).{8,}$")
            return regex.matches(contrasena)
        }

    }