package com.example.eduapp.ui.compose.estudiantes

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import com.example.eduapp.R
import dagger.hilt.android.AndroidEntryPoint
import com.example.eduapp.databinding.FragmentComposeFragmentEsBinding
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.ScaffoldState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import coil.compose.rememberImagePainter
import com.example.eduapp.domain.model.StudentDomain
import com.example.eduapp.ui.compose.estudiantes.navigationStudent.StudentNavigation
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.untils_ui.MaterialButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

import com.jakewharton.threetenabp.AndroidThreeTen
import kotlinx.coroutines.delay

@AndroidEntryPoint
class Compose_fragment_es : Fragment() {

    private var _binding: FragmentComposeFragmentEsBinding? = null
    private val binding get() = _binding!!

    private val studentviewModel: ViewModelStudent by viewModels()

    private val profesorviewModel: ViewModelProfesor by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentComposeFragmentEsBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(R.id.compose_fragment_es)
        }

        AndroidThreeTen.init(requireContext())

        val compose = binding.ComposeContainer
        compose.setContent {
            viewContainer()
        }

        studentviewModel.istOnline.observe(viewLifecycleOwner) { success ->
            if (!success) Toast.makeText(this.context, "No tiene conexion a internet para actualizar los datos", Toast.LENGTH_SHORT)
                .show()
        }
    }


    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    fun viewContainer() {

        val userInfoState by studentviewModel.user.collectAsState(initial = emptyList())

        LaunchedEffect(Unit) {
            studentviewModel.getUser()
        }


        var scaffoldState = rememberScaffoldState()
        var scope = rememberCoroutineScope()
        val context = LocalContext.current

        val isLoading by studentviewModel.isLoadingFlow.collectAsState(initial = false)


        Scaffold(
            scaffoldState = scaffoldState,
            topBar = {
                if (isLoading) Column (modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp)
                    .background(colorResource(id = R.color.white))
                ){

                }
                else CustomTopAppBarEs(scope, scaffoldState, studentviewModel) },
            drawerContent = { Drawer(userInfoState) },
            content = {
                StudentNavigation(userInfoState, studentviewModel, profesorviewModel)
            }
        )
    }


    @Composable
    private fun Drawer(
        userInfoState: List<StudentDomain>
    ) {


        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Perfil",
                    style = TextStyle(fontSize = 24.sp)
                )
            }

            userInfoState.forEach { user ->

                ImageViewWithBase64(base64String = user.foto)

                Text(
                    text = "Nombre: ${user.nombre}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )

                Text(
                    text = "Correo: ${user.correo}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )
                Text(
                    text = "Edad: ${user.fechaNacimiento}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(16.dp)
                )
                Text(
                    text = "edad: ${user.edad}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(start = 5.dp, end = 5.dp)
                )
                Text(
                    text = "Sexo: ${user.sexo}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )
                Text(
                    text = "Teléfono: ${user.telefono}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )
            }
        }
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(16.dp),
            verticalArrangement = Arrangement.Bottom
        ) {
            MaterialButton(text = "Editar") {
                findNavController().navigate(R.id.action_compose_fragment_es_to_editarPerfilFragmentEs)
            }
        }
    }


    @Composable
    fun CustomTopAppBarEs(
        scope: CoroutineScope,
        scaffoldState: ScaffoldState,
        profesorviewModel: ViewModelStudent,
    ) {

        var showDialog by remember { mutableStateOf(false) }

        var shouldNavigate by remember { mutableStateOf(false) }

        LaunchedEffect(shouldNavigate) {
            if (shouldNavigate) {
                delay(1000)
                profesorviewModel.logout()
                findNavController().navigate(R.id.action_compose_fragment_es_to_mainFragment)
            }
        }

        if (showDialog) {
            AlertDialog(
                onDismissRequest = { showDialog = false }, // Desactiva el diálogo si se cancela
                title = {
                    Text(text = "Confirmación de cierre de sesión")
                },
                text = {
                    Text(text = "¿Desea cerrar sesión?")
                },
                confirmButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colorResource(id = R.color.orange) // Configura el color de fondo del botón de confirmación
                        ),
                        onClick = {
                            profesorviewModel.logout()
                            shouldNavigate = true
                            showDialog = false
                        }
                    ) {
                        Text("Sí")
                    }
                },
                dismissButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colorResource(id = R.color.orange) // Configura el color de fondo del botón de confirmación
                        ),
                        onClick = {
                            showDialog = false
                        }
                    ) {
                        Text("No")
                    }
                }
            )
        }

        TopAppBar(
            title = {
                Text(
                    text = "Eduapp",
                    color = colorResource(id = R.color.white),
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            },
            navigationIcon = {
                IconButton(onClick = {
                    scope.launch {
                        scaffoldState.drawerState.open()
                    }
                }) {
                    Icon(
                        imageVector = Icons.Filled.AccountCircle,
                        contentDescription = "Icono de menu",
                        tint = colorResource(id = R.color.white)
                    )
                }
            },
            actions = {
                IconButton(onClick = {
                    showDialog = true
                }) {
                    Icon(
                        imageVector = Icons.Filled.ExitToApp,
                        contentDescription = "Buscar",
                        tint = colorResource(id = R.color.white)
                    )
                }
            },
            backgroundColor = colorResource(id = R.color.orange)
        )

    }

}

    fun decodificarFotoBase64(base64String: String): Bitmap? {
        val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
        return if (decodedBytes.isNotEmpty()) {
            BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
        } else {
            null
        }
    }

    @Composable
    fun ImageViewWithBase64(base64String: String, shape: Shape = CircleShape) {
        Box(
            modifier = Modifier
                .size(200.dp)
        ) {
            val bitmap: Bitmap? = decodificarFotoBase64(base64String)
            bitmap?.let {
                Image(
                    painter = rememberImagePainter(data = bitmap),
                    contentDescription = null,
                    modifier = Modifier.fillMaxSize(),
                    contentScale = androidx.compose.ui.layout.ContentScale.Crop
                )
            }
        }
    }



