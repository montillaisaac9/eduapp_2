package com.example.eduapp.ui.xml.editar

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.domain.auth.UseCaseAuth
import com.example.eduapp.domain.model.NewProfesorDomain
import com.example.eduapp.domain.model.NewStudentDomain
import com.example.eduapp.untils.isOnline
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EditarPerfilViewModel @Inject constructor(
    private val useCaseAuth: UseCaseAuth
) : ViewModel() {

    val userLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val istOnline: MutableLiveData<Boolean> = MutableLiveData()
    val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val errorLiveData: MutableLiveData<String> = MutableLiveData()

    private val _userPr = MutableStateFlow<List<UserProfesorEntity>>(emptyList())
    val userPr: StateFlow<List<UserProfesorEntity>> = _userPr

    private val _userEs = MutableStateFlow<List<UserStudentEntity>>(emptyList())
    val userEs: StateFlow<List<UserStudentEntity>> = _userEs

    fun getUser() {
        viewModelScope.launch {
            useCaseAuth.getProfesor().collect {
                _userPr.value = it
            }
        }
    }

    fun cambiarPerfilPr(
        id: Int,
        email: String,
        password: String,
        nombre: String,
        telefono: String,
        rol: String,
        requireContext: Context
    ) {
        if (isOnline(requireContext)) {
        val newProfesorDomain = NewProfesorDomain(
            correo = email,
            contraseña = password,
            nombre = nombre,
            telefono = telefono
        )
            istOnline.postValue(false)
            guardarPr(id, newProfesorDomain)
        } else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
            isLoadingLiveData.postValue(false)
        }
    }

    fun cambiarEs(
        id: String,
        nombre: String,
        email: String,
        password: String,
        fecha: String,
        base64String: String?,
        sexoSeleccionado: String?,
        telefono: String,
        latitude: String?,
        longitude: String?,
        requireContext: Context
    ) {
        if (isOnline(requireContext)) {
            istOnline.postValue(false)

        val newStudentDomain = NewStudentDomain(
            nombre = nombre,
            correo = email,
            contraseña = password,
            fechaNacimiento = fecha,
            foto = base64String,
            sexo = sexoSeleccionado,
            telefono = telefono,
            latitud = latitude,
            longitud = longitude,
            materias = emptyList()
        )
            guardarEs(id, newStudentDomain)
        } else {
            Log.e("no", "no tengo internet boludo")
            istOnline.postValue(true)
            isLoadingLiveData.postValue(false)
        }
    }


    private fun guardarEs(id: String, newStudentDomain: NewStudentDomain) {
        viewModelScope.launch {
            isLoadingLiveData.postValue(true)
            val result = useCaseAuth.editStudent(id, newStudentDomain)
            Log.i("inicio", "logrado")
            isLoadingLiveData.postValue(false)
            if (result) userLiveData.postValue(true)
            else {
                userLiveData.postValue(false)
                isLoadingLiveData.postValue(false)
            }

        }
    }

    private fun guardarPr(id: Int, newProfesorDomain: NewProfesorDomain) {
        viewModelScope.launch {
            val result = useCaseAuth.editProfesor(id, newProfesorDomain)
            Log.i("inicio", "logrado")
            if (result) userLiveData.postValue(true)
            else {
                userLiveData.postValue(false)
                isLoadingLiveData.postValue(false)
            }

        }
    }

    fun getUserEs() {
        viewModelScope.launch {
            useCaseAuth.getStudent().collect {
                _userEs.value = it
            }
        }
    }
}