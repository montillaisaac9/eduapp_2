package com.example.eduapp.ui.compose.profesores.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.eduapp.R
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.profesores.navigationProfesor.ProfesorScreen
import com.example.eduapp.ui.compose.untils_ui.MaterialButton


@Composable
fun detallesAsignaturas(
    profesorviewModel: ViewModelProfesor,
    id: Int?,
    navController: NavHostController
) {

    val asignatura by profesorviewModel.asignaturaDetails.collectAsState(initial = null)

    LaunchedEffect(id) {
        if (id != 0) {
            id?.let { profesorviewModel.getAsignaturaById(it)}
        }
    }


    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(20.dp)
            .background(color = colorResource(id = R.color.white)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        asignatura?.let {
            Text(
                text = it.nombre,
                style = TextStyle(fontSize = 32.sp),
                modifier = Modifier.padding(16.dp)
            )
        }

        MaterialButton(text ="Ver listado de Estudiantes") {
            navController.navigate("${ProfesorScreen.EstudiantesInscritosScreen.routes}/${id}")
        }
        Spacer(modifier = Modifier.height(32.dp))

        Line()
        Spacer(modifier = Modifier.height(32.dp))
        MaterialButton(text ="Actividades") {
            navController.navigate("${ProfesorScreen.ActividadesScreen.routes}/${id}")
        }
        Spacer(modifier = Modifier.height(32.dp))
        Line()
        Spacer(modifier = Modifier.height(32.dp))
        MaterialButton(text ="Estadisticas") {
            navController.navigate("${ProfesorScreen.EstadisticaProfesor.routes}/${id}")
        }
        Spacer(modifier = Modifier.height(32.dp))
        Line()
        Spacer(modifier = Modifier.height(32.dp))
    }

}

@Composable
fun Line() {
    Box(
        modifier = Modifier
            .width(300.dp)
            .height(1.dp)
            .background(color = Color.Black)

    )
}