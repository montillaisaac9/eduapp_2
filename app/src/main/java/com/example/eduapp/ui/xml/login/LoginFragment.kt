package com.example.eduapp.ui.xml.login

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.bumptech.glide.Glide
import com.example.eduapp.R
import com.example.eduapp.databinding.FragmentLoginBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val bgmain = binding.imageBg
        val imgIconBg = R.drawable.img
        Glide.with(this).load(imgIconBg).centerCrop().into(bgmain)

        val imgIconMain = binding.imageICONMain
        val imgIconMainbg = R.drawable.logo_size
        Glide.with(this).load(imgIconMainbg).fitCenter().into(imgIconMain)

        binding.editTextEmail.textCursorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.cursor_drawable)

        initEvents()
        subcribeObeserver()
    }

    private fun initEvents(){
        binding.t2register.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment2)
            binding.t2register.isEnabled = false
        }

        binding.btnLogin.setOnClickListener { login() }
    }

    private fun subcribeObeserver () {



        loginViewModel.itsEmpy.observe(viewLifecycleOwner) { success ->
            if (success) Toast.makeText(this.context, "los campos estan vacios", Toast.LENGTH_SHORT)
                .show()
        }

        loginViewModel.istOnline.observe(viewLifecycleOwner) { success ->
            if (success) Toast.makeText(this.context, "No tiene conexion a internet para Iniciar Secion", Toast.LENGTH_SHORT)
                .show()
        }

        loginViewModel.isLoadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            binding.btnLogin.isEnabled = !isLoading
            binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        loginViewModel.studentLoginResultLiveData.observe(viewLifecycleOwner) { success ->
            if (success) {
                Toast.makeText(this.context, "Ha iniciado sesión con éxito como estudiante", Toast.LENGTH_SHORT)
                    .show()

                // Configurar NavOptions con popUpTo para eliminar loginFragment de la pila de retroceso
                val navOptions = navOptions {
                    popUpTo(R.id.loginFragment) {
                        inclusive = true
                    }
                }

                // Navegar a compose_fragment_es con NavOptions
                findNavController().navigate(R.id.action_loginFragment_to_compose_fragment_es, null, navOptions)
            } else {
                Toast.makeText(this.context, "El usuario no está registrado", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        loginViewModel.profesorLoginResultLiveData.observe(viewLifecycleOwner) { success ->
            if (success) {
                Toast.makeText(this.context, "Ha iniciado sesión con éxito como profesor", Toast.LENGTH_SHORT)
                    .show()

                // Configurar NavOptions con popUpTo para eliminar loginFragment de la pila de retroceso
                val navOptions = navOptions {
                    popUpTo(R.id.loginFragment) {
                        inclusive = true
                    }
                }

                // Navegar a compose_fragment_pr con NavOptions
                findNavController().navigate(R.id.action_loginFragment_to_compose_fragment_pr, null, navOptions)
            } else {
                Toast.makeText(this.context, "El usuario no está registrado", Toast.LENGTH_SHORT)
                    .show()
            }
        }

    }
    private fun login() {

        val rol = binding.swichtRole.isChecked
        val email = binding.editTextEmail.text.toString()
        val pass = binding.editTextPassword.text.toString()

        loginViewModel.loginAuth(email, pass, rol, requireContext())

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
