package com.example.eduapp.ui.compose.profesores.screens

import android.Manifest
import android.app.Activity
import android.content.ContentProviderOperation
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.ContactsContract
import android.util.Base64
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Call
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import coil.compose.rememberImagePainter
import com.example.eduapp.R
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.*
import androidx.compose.ui.window.Dialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.example.eduapp.ui.compose.profesores.Compose_fragment_pr
import com.google.android.gms.maps.model.CameraPosition



@Composable
fun DetallesEstudiante(
    profesorviewModel: ViewModelProfesor,
    idEstudiante: String?,
    navController: NavHostController
) {

    val studentInfoState by profesorviewModel.detallesEstudiante.collectAsState(initial = emptyList())

    val isLoading by profesorviewModel.isLoadingFlow.collectAsState(initial = false)

    val context = LocalContext.current



    val fragmentManager = (context as? FragmentActivity)?.supportFragmentManager


    LaunchedEffect(idEstudiante) {
        idEstudiante?.let { profesorviewModel.getStudent(it, context) }
    }



    // Estado para controlar la visibilidad del diálogo con el mapa
    var showMapDialog by remember { mutableStateOf(false) }
    var latitud by remember { mutableStateOf(0.0) }
    var longitud by remember { mutableStateOf(0.0) }

    //intent llamar
    var llamada by remember { mutableStateOf(false) }
    var telefono by remember { mutableStateOf( "") }

    if (isLoading) ProgressBarCompose()
    else {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
                .background(color = colorResource(id = R.color.white)),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Perfil",
                    style = TextStyle(fontSize = 24.sp)
                )
            }

            studentInfoState.forEach { user ->

                ImageViewWithBase64(base64String = user.foto)

                Text(
                    text = "Nombre: ${user.nombre}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )

                Text(
                    text = "Correo: ${user.correo}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )
                Text(
                    text = "fecha de nacimiento: ${user.fechaNacimiento}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(start = 5.dp, end = 5.dp)
                )
                Text(
                    text = "Sexo: ${user.sexo}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )
                Text(
                    text = "Teléfono: ${user.telefono}",
                    style = TextStyle(fontSize = 14.sp),
                    modifier = Modifier.padding(14.dp)
                )
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    // Botón para realizar llamada
                    Surface(
                        onClick = {
                            telefono = user.telefono
                            llamada = true
                        },
                        color = colorResource(id = R.color.orange),
                        shape = CircleShape,
                        modifier = Modifier.padding(20.dp)
                    ) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier.padding(12.dp)
                        ) {
                            Icon(
                                imageVector = Icons.Default.Call,
                                contentDescription = "Llamar",
                                tint = Color.White,
                                modifier = Modifier.padding(end = 8.dp)
                            )
                            Text(
                                text = "Llamar",
                                style = TextStyle(fontSize = 14.sp, color = Color.White)
                            )
                        }
                    }

                    // Botón para mostrar el diálogo con el mapa
                    Surface(
                        onClick = {
                            latitud = user.latitud.toDouble()
                            longitud = user.longitud.toDouble()
                            showMapDialog = true
                        },
                        color = colorResource(id = R.color.orange),
                        shape = CircleShape,
                        modifier = Modifier.padding(20.dp)
                    ) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier.padding(12.dp)
                        ) {
                            Icon(
                                imageVector = Icons.Default.LocationOn,
                                contentDescription = "Ubicación",
                                tint = Color.White,
                                modifier = Modifier.padding(end = 8.dp)
                            )
                            Text(
                                text = "Ubicación",
                                style = TextStyle(fontSize = 14.sp, color = Color.White)
                            )
                        }
                    }
                }
            }
            if (llamada){
                CallPhone(telefono)
                llamada = false
            }
            if (showMapDialog) {
                ShowMapDialog(
                    lat = latitud,
                    lng = longitud,
                    onDismiss = { showMapDialog = false }
                )
            }
        }
    }
}

@Composable
fun CallPhone(phoneNumber: String) {
    val context = LocalContext.current
    var hasCallPermission by remember {
        mutableStateOf(
            ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.CALL_PHONE
            ) == PackageManager.PERMISSION_GRANTED
        )
    }

    // Crea un `ActivityResultLauncher` para solicitar el permiso de llamada.
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission()
    ) { granted ->
        hasCallPermission = granted
        if (!granted) {
            Toast.makeText(context, "Permiso de llamada denegado", Toast.LENGTH_SHORT).show()
        }
    }

    // Lanzar efecto para solicitar permisos al inicio si no están concedidos.
    LaunchedEffect(key1 = true) {
        if (!hasCallPermission) {
            launcher.launch(Manifest.permission.CALL_PHONE)
        }
    }

    // Realizar la llamada si se tiene el permiso.
    if (hasCallPermission) {
        val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$phoneNumber"))
        context.startActivity(callIntent)
    }
}


@Composable
fun ShowMapDialog(lat: Double, lng: Double, onDismiss: () -> Unit) {
    Dialog(onDismissRequest = onDismiss) {
        Surface(
            modifier = Modifier.size(300.dp, 300.dp)
        ) {
            val cameraPositionState = rememberCameraPositionState {
                position = CameraPosition(LatLng(lat, lng), 15f, 0f, 0f)
            }


            GoogleMap(
                cameraPositionState = cameraPositionState
            ) {
                Marker(
                    state = MarkerState(position = LatLng(lat, lng)),
                    title = "Ubicación del usuario"
                )
            }
        }
    }
}

fun decodificarFotoBase64(base64String: String): Bitmap? {
    val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
    return if (decodedBytes.isNotEmpty()) {
        BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
    } else {
        null
    }
}

@Composable
fun ImageViewWithBase64(base64String: String, shape: Shape = CircleShape) {
    Box(
        modifier = Modifier.size(200.dp)
    ) {
        val bitmap: Bitmap? = decodificarFotoBase64(base64String)
        bitmap?.let {
            Image(
                painter = rememberImagePainter(data = bitmap),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = androidx.compose.ui.layout.ContentScale.Crop
            )
        }
    }
}

