package com.example.eduapp.ui.compose.estudiantes.screens

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.eduapp.R
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.StudentDomain
import com.example.eduapp.ui.compose.estudiantes.ViewModelStudent
import com.example.eduapp.ui.compose.untils_ui.ProgressBarCompose
import kotlinx.coroutines.CoroutineScope
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter


@Composable
fun ActividadesEstudiantesScreen(
    studentviewModel: ViewModelStudent,
    userInfoState: List<StudentDomain>,
    id_asignatura: Int?,
    navController: NavHostController
) {
    val asignatura by studentviewModel.asignaturaDetails.collectAsState(initial = null)


    var id_user by remember { mutableStateOf("") }

    LaunchedEffect(userInfoState) {
        userInfoState.forEach {
            id_user = it.id
        }
    }

    LaunchedEffect(id_asignatura) {
            id_asignatura?.let { studentviewModel.getAsignaturaById(it)}
    }

    val context = LocalContext.current

    var scope = rememberCoroutineScope()

    val actividades by studentviewModel.actividades.collectAsState(initial = emptyList())

    val notas by studentviewModel.notaMostrar.collectAsState(initial = emptyList())

    val estaCreada by studentviewModel.notaCreada.collectAsState(initial = false)

    if (estaCreada) Toast.makeText(context, "esta actividad ya se entrego", Toast.LENGTH_SHORT).show()

    LaunchedEffect(id_asignatura) {
            id_asignatura?.let { studentviewModel.getActivities(it, context) }
    }

    val isLoading by studentviewModel.isLoadingFlow.collectAsState(initial = false)

    if (isLoading) ProgressBarCompose()
    else ContentStudenteActivies(asignatura, actividades, context, scope, studentviewModel, navController, id_user, notas)

}

@Composable
fun ContentStudenteActivies(
    asignatura: AsignaturaDomain?,
    actividades: List<ActividadDomain?>,
    context: Context,
    scope: CoroutineScope,
    studentviewModel: ViewModelStudent,
    navController: NavHostController,
    id_user: String,
    notas: List<NotaDomain?>
) {

    Column (
        modifier = Modifier
            .fillMaxSize()
            .background(color = colorResource(id = R.color.white)),
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Text(
            text = "Actividades de la Asignatura ${asignatura?.nombre}",
            style = TextStyle(fontSize = 20.sp),
            modifier = Modifier.padding(16.dp)
        )

        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(actividades.size) { index ->
                val actividades = actividades[index]
                actividades?.let { ListActividadesRowEs(it, context, navController, studentviewModel, id_user, notas) }
            }
        }
    }
}

@Composable
fun ListActividadesRowEs(
    actividad: ActividadDomain,
    context: Context,
    navController: NavController,
    studentviewModel: ViewModelStudent,
    id_user: String,
    notas: List<NotaDomain?>
) {
    val coroutineScope = rememberCoroutineScope()
    var expanded by remember { mutableStateOf(false) }
    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")

    val esDespues = esFechaDespues(actividad.fecha_limit)
    Log.e("es depueses", esDespues.toString())
    // Estado para habilitar o deshabilitar el botón de entregar
    var buttonEnabled by remember { mutableStateOf(esDespues) }


    val currentDate: LocalDateTime = LocalDateTime.now()
    val formattedDate: String = formatter.format(currentDate)


    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 20.dp)
            .background(
                color = colorResource(id = R.color.gray),
                shape = RoundedCornerShape(8.dp) // Agregar la forma redondeada
            )
            .clickable {
                expanded = !expanded
            }
            .animateContentSize()
    ) {
        if (expanded) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Column (
                    modifier = Modifier
                        .padding(10.dp)
                ) {
                    Row (
                        modifier = Modifier.fillMaxWidth(),
                        Arrangement.SpaceBetween
                    ){
                        Text(
                            text = actividad.nombre,
                            modifier = Modifier
                                .padding(vertical = 16.dp, horizontal = 30.dp),
                            style = TextStyle(fontSize = 24.sp)
                        )
                        Text(
                            text = "descripcion: ${actividad.descripcion}",
                            modifier = Modifier
                                .padding(vertical = 20.dp, horizontal = 30.dp),
                            style = TextStyle(fontSize = 15.sp)
                        )
                    }
                    Text(
                        text = "Puntuación máxima: ${actividad.puntuacion_Max}",
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentSize(Alignment.Center)
                            .padding(vertical = 16.dp),
                        style = TextStyle(fontSize = 15.sp)
                    )

                    Text(
                        text = "Fecha límite: ${actividad.fecha_limit}",
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentSize(Alignment.Center)
                            .padding(vertical = 16.dp),
                        style = TextStyle(fontSize = 15.sp)
                    )
                    notas.forEach{ it->
                        Log.e("te traje esto", it?.puntuacion.toString())
                        if (it != null) {
                            if (it.actividadId == actividad.id && it.puntuacion != null){
                                Text(
                                    text = "Tu Puntuacion Es: ${it.puntuacion}",
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .wrapContentSize(Alignment.Center)
                                        .padding(vertical = 16.dp),
                                    style = TextStyle(fontSize = 15.sp)
                                )
                            }
                        }
                    }
                    Row (
                        modifier = Modifier.fillMaxWidth(),
                        Arrangement.SpaceBetween
                    ) {
                        Button(
                            colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange)),
                            onClick = {
                                if (buttonEnabled) {
                                    studentviewModel.crearNota(actividad.id, actividad.asignatura_id, id_user, formattedDate, context)
                                }
                            },
                            modifier = Modifier
                                .height(32.dp),
                            enabled = buttonEnabled
                        ) {
                            Text(text = "Entregar")
                        }
                        Button(
                            colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange)),
                            onClick = {
                                    studentviewModel.obtenerNota(actividad.id, actividad.asignatura_id, id_user, formattedDate, context)
                            },
                            modifier = Modifier
                                .height(32.dp)
                        ) {
                            Text(text = "Ver Nota")
                        }
                    }
                }
            }
        } else {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = actividad.nombre,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(vertical = 16.dp)
                        .fillMaxWidth(),
                    style = TextStyle(fontSize = 24.sp)
                )
            }
        }
    }
}

fun esFechaDespues(fecha: String): Boolean {

    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
    val fechaRecibida = LocalDate.parse(fecha, formatter)
    val fechaActual = LocalDate.now()
    return if (fechaActual.isBefore(fechaRecibida) || fechaActual.isEqual(fechaRecibida)) true
    else false
}