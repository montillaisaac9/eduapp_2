@file:Suppress("DEPRECATION")

package com.example.eduapp.ui.xml.editar.estudiante

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.eduapp.R
import com.example.eduapp.databinding.FragmentEditarPerfilEsBinding
import com.example.eduapp.ui.xml.completar_perfil.CompletarPerfilViewModel
import com.example.eduapp.ui.xml.editar.EditarPerfilViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

@AndroidEntryPoint
class EditarPerfilFragmentEs() : Fragment() {

    private val LOCATION_PERMISSION_REQUEST_CODE = 1001

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    lateinit var id: String
    lateinit var email: String
    lateinit var nombre: String
    lateinit var contraseña: String
    lateinit var fecha: String
    lateinit var telefono: String
    lateinit var sexoSeleccionado: String
    lateinit var latitude: String
    lateinit var longitude: String
    lateinit var base64String: String


    private var _binding: FragmentEditarPerfilEsBinding? = null
    private val binding get() = _binding!!

    private lateinit var editPerfilViewModel: EditarPerfilViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEditarPerfilEsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())


        editPerfilViewModel = ViewModelProvider(this)[EditarPerfilViewModel::class.java]
        editPerfilViewModel.getUserEs()


        lifecycleScope.launch {
            editPerfilViewModel.userEs.collect { userList ->
                if (userList.isNotEmpty()) {
                    id = userList[0].id.toString()
                    email = userList[0].correo
                    contraseña = userList[0].contraseña
                    nombre =  userList[0].nombre
                    base64String =  userList[0].foto
                    fecha =  userList[0].fechaNacimiento
                    sexoSeleccionado = userList[0].sexo
                    latitude = userList[0].latitud
                    longitude = userList[0].latitud
                    binding.editTextname.setText(userList[0].nombre.toString())

                    if (sexoSeleccionado == "Masculino") binding.masculinoRadioButton.isChecked = true
                    else binding.femeninoRadioButton.isChecked = true

                    binding.fecha.text = fecha

                    binding.editTextTelefono.setText(userList[0].telefono.toString())
                    if (userList[0].foto.isNotEmpty()) {
                        val imageBytes = Base64.decode(userList[0].foto, Base64.DEFAULT)
                        val bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                        val imageView = binding.Photo
                        imageView.setImageBitmap(bitmap)
                    }
                } else {
                    binding.editTextname.setText("")
                }
            }
        }

        initEvents()
        onsubcribeOberser()
    }

    private fun initEvents() {
        // Instancia de ImageSelector
        val imageSelector = ImageSelector(this)

        // Configuración de fecha
        val calendario = Calendar.getInstance()
        val fecha = DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
            calendario.set(Calendar.YEAR, year)
            calendario.set(Calendar.MONTH, month)
            calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            actualizar_fecha(calendario)
            // Llama a la función para activar el botón
            activarBtn()
        }

        // Configuración de los eventos de los botones
        binding.btnSelecionarFecha.setOnClickListener {
            it.isEnabled = false
            DatePickerDialog(
                requireContext(),
                fecha,
                calendario.get(Calendar.YEAR),
                calendario.get(Calendar.MONTH),
                calendario.get(Calendar.DAY_OF_MONTH)
            ).show()

            // Llama a la función para activar el botón
            activarBtn()

            // Retrasa la habilitación del botón después de 1 segundo
            CoroutineScope(Dispatchers.Main).launch {
                delay(1000)
                it.isEnabled = true
            }
        }

        binding.btnGuardar.setOnClickListener {
            it.isEnabled = false
            editar()
        }

        binding.btnAddPhoto.setOnClickListener {
            it.isEnabled = false
            imageSelector.showImageSelectionDialog()
            // Retrasa la habilitación del botón después de 1 segundo
            CoroutineScope(Dispatchers.Main).launch {
                delay(1000)
                it.isEnabled = true
                // Llama a la función para activar el botón
                activarBtn()
            }
        }

        binding.btnUbicacion.setOnClickListener {
            it.isEnabled = false
            obtenerUbicacion()
            // Llama a la función para activar el botón
            activarBtn()
            // Retrasa la habilitación del botón después de 1 segundo
            CoroutineScope(Dispatchers.Main).launch {
                delay(1000)
                it.isEnabled = true
                activarBtn()
            }
        }

        // Configuración de TextWatcher para los EditText
        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Puedes realizar acciones antes de que el texto cambie, si lo deseas.
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Puedes realizar acciones mientras el texto cambia, si lo deseas.
            }

            override fun afterTextChanged(s: Editable?) {
                // Llama a la función para activar el botón
                activarBtn()
            }
        }

        // Añadir TextWatcher a los EditText
        binding.editTextname.addTextChangedListener(textWatcher)
        binding.editTextTelefono.addTextChangedListener(textWatcher)

        // Configuración de FocusChangeListener para los EditText
        binding.editTextname.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                activarBtn()
            }
        }

        binding.editTextTelefono.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                activarBtn()
            }
        }

        // Configuración del listener para el RadioGroup
        val radioGroup = binding.radioGroup
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.masculinoRadioButton -> {
                    sexoSeleccionado = "Masculino"
                    // Llama a la función para activar el botón
                    activarBtn()
                }
                R.id.femeninoRadioButton -> {
                    sexoSeleccionado = "Femenino"
                    // Llama a la función para activar el botón
                    activarBtn()
                }
                else -> Toast.makeText(context, "Error: elija un sexo", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun requestLocationPermission() {
        requestPermissions(
            arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
            LOCATION_PERMISSION_REQUEST_CODE
        )
    }


    private fun obtenerUbicacion() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location ->
                    if (location != null) {
                        latitude = location.latitude.toString()
                        longitude = location.longitude.toString()
                        activarBtn()
                        binding.ubicacion.text = "obtenida"
                    } else {
                        Toast.makeText(this.context, "error al obtener la ubicacion", Toast.LENGTH_SHORT).show()
                    }
                }
                .addOnFailureListener { exception ->
                    Log.e("error", "${exception}")
                    Toast.makeText(this.context, "error al obtener la ubicacion", Toast.LENGTH_SHORT).show()
                }
        } else {
            requestLocationPermission()
        }

    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                obtenerUbicacion()
            } else {
                Toast.makeText(this.context, "error al obtener la ubicacion", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun actualizar_fecha(calendar: Calendar){
        val formatoFecha = "dd-MM-yyyy"
        val formatoSimple = SimpleDateFormat(formatoFecha, Locale.ENGLISH)
        binding.fecha.text = formatoSimple.format(calendar.time)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val imageView = binding.Photo
            when (requestCode) {
                ImageSelector.REQUEST_IMAGE_CAPTURE -> {
                    // La imagen fue capturada desde la cámara
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    setImageWithGlide1(imageView, imageBitmap)
                }
                ImageSelector.REQUEST_SELECT_IMAGE -> {
                    // La imagen fue seleccionada desde la galería
                    val imageUri = data?.data
                    setImageWithGlide2(imageView, imageUri)
                }
            }
        }
    }
    private fun setImageWithGlide1(imageView: ImageView, imageBitmap: Bitmap) {
        Glide.with(this)
            .load(imageBitmap)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)

        val bitmapDrawable = (binding.Photo.drawable as? BitmapDrawable)
        if (bitmapDrawable != null) {
            val bitmap = bitmapDrawable.bitmap


            if (esFotoValida(bitmap)) {
                base64String = convertirFotoABase64(bitmap)
                Toast.makeText(context, "foto selecionada espera unos segundos para manejarla", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "No se ha seleccionado una foto válida", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(context, "No se ha seleccionado una foto", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setImageWithGlide2(imageView: ImageView, imageUri: Uri?) {
        Glide.with(this)
            .load(imageUri)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }

    private fun activarBtn() {
        val nombre = binding.editTextname.text.toString()
        val fecha = binding.fecha.text.toString() // Asegúrate de que sea un string
        val telefono = binding.editTextTelefono.text.toString()

        val bitmapDrawable = (binding.Photo.drawable as? BitmapDrawable)
        if (bitmapDrawable != null) {
            val bitmap = bitmapDrawable.bitmap
            if (esFotoValida(bitmap)) {
                base64String = convertirFotoABase64(bitmap)
            }
        }

        val condiciones = listOf(
            nombre.isNotEmpty(),
            fecha != "dd/mm/yyyy",
            telefono.isNotEmpty(),
            base64String != null,
            sexoSeleccionado != null,
            longitude != null && latitude != null
        )


        binding.btnGuardar.isEnabled = condiciones.all { it }
    }

    fun esFotoValida(bitmap: Bitmap?): Boolean {
        return bitmap != null && bitmap.width > 0 && bitmap.height > 0
    }
    fun convertirFotoABase64(bitmap: Bitmap): String {

        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()

        val base64String: String = Base64.encodeToString(byteArray, Base64.DEFAULT)

        return base64String
    }

    fun editar(){
        val nombreEditar = binding.editTextname.text.toString()
        val fechaEditar = binding.fecha.text.toString()
        val telefonoEditar = binding.editTextTelefono.text.toString()
        val rol = "student"

        editPerfilViewModel.cambiarEs(id, nombreEditar, email, contraseña, fechaEditar, base64String, sexoSeleccionado, telefonoEditar, longitude, latitude, requireContext())
    }

    fun onsubcribeOberser(){
        editPerfilViewModel.istOnline.observe(viewLifecycleOwner) { success ->
            if (success) Toast.makeText(this.context, "No tiene conexion a internet para Iniciar Secion", Toast.LENGTH_SHORT)
                .show()
        }

        editPerfilViewModel.userLiveData.observe(viewLifecycleOwner){
            if (it) {
                Toast.makeText(this.context, "se estan Guardando los cambios del perfil del estudiante...", Toast.LENGTH_SHORT)
                findNavController().navigate(R.id.action_editarPerfilFragmentEs_to_compose_fragment_es)}
        }

        editPerfilViewModel.isLoadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
            binding.btnGuardar.isEnabled = !isLoading
        }
    }


}


