package com.example.eduapp.ui.xml.completar_perfil.profesor

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.eduapp.R
import dagger.hilt.android.AndroidEntryPoint
import com.example.eduapp.databinding.FragmentCompletarPerfilPrBinding
import com.example.eduapp.ui.xml.completar_perfil.CompletarPerfilViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CompletarPerfilFragmentPr : Fragment() {

    private lateinit var email: String
    private lateinit var password: String

    private var _binding: FragmentCompletarPerfilPrBinding? = null
    private val binding get() = _binding!!

    private lateinit var completePerilViewModel: CompletarPerfilViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCompletarPerfilPrBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        email = arguments?.getString("email").toString()
        password = arguments?.getString("password").toString()

        completePerilViewModel = ViewModelProvider(this)[CompletarPerfilViewModel::class.java]

        initEvents()
        onsubcribeOberser()

    }

    private fun onsubcribeOberser() {
        completePerilViewModel.istOnline.observe(viewLifecycleOwner) { success ->
            if (success) Toast.makeText(this.context, "No tiene conexion a internet para Iniciar Secion", Toast.LENGTH_SHORT)
                .show()
        }

        completePerilViewModel.userLiveData.observe(viewLifecycleOwner){
            if (it) {
                Toast.makeText(this.context, "Felicidades ahora eres un estudiante registrado Logeate", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_completarPerfilFragmentPr_to_loginFragment)}
        }

        completePerilViewModel.isLoadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
            binding.btnGuardar.isEnabled = !isLoading
        }
    }

    private fun initEvents() {
        // Configura el evento de cambio de texto para editTextTelefono
        binding.editTextTelefono.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // No se necesita hacer nada aquí
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Llama a activarBtn cuando cambie el texto
                activarBtn()
            }

            override fun afterTextChanged(s: Editable?) {
                // No se necesita hacer nada aquí
            }
        })

        // Configura el evento de pérdida de foco para editTextTelefono
        binding.editTextTelefono.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                activarBtn()
            }
        }

        // Configura el evento de clic para btnGuardar
        binding.btnGuardar.setOnClickListener {
            it.isEnabled = false
            guardar()
            CoroutineScope(Dispatchers.Main).launch {
                delay(1000)
                it.isEnabled = true
            }
        }
    }


    private fun activarBtn() {
        binding.btnGuardar.isEnabled = false
        var nombre = binding.editTextname.text.toString()
        var telefono = binding.editTextTelefono.text.toString()

        if (nombre.isEmpty()) {

        } else if (telefono.isEmpty()) {

        } else {
            binding.btnGuardar.isEnabled = true
        }
    }

    fun guardar(){
        var nombre = binding.editTextname.text.toString()
        var telefono = binding.editTextTelefono.text.toString()
        var rol = "profesor"
        Toast.makeText(context, "Felicidades te has registrado como profesor", Toast.LENGTH_SHORT).show()
        completePerilViewModel.registrarPr(email, password, nombre, telefono, rol, requireContext())
    }
}