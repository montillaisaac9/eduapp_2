package com.example.eduapp.ui.compose.estudiantes.navigationStudent

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.eduapp.domain.model.StudentDomain
import com.example.eduapp.ui.compose.estudiantes.ViewModelStudent
import com.example.eduapp.ui.compose.estudiantes.screens.ActividadesEstudiantesScreen
import com.example.eduapp.ui.compose.estudiantes.screens.EstadisticasEs
import com.example.eduapp.ui.compose.estudiantes.screens.ScreenAsignaturasStudent
import com.example.eduapp.ui.compose.profesores.ViewModelProfesor


@Composable
fun StudentNavigation(
    userInfoState: List<StudentDomain>,
    studentviewModel: ViewModelStudent,
    profesorviewModel: ViewModelProfesor
) {

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = StudentScreens.studentAsignaturaScreen.routes) {
        composable(route = StudentScreens.studentAsignaturaScreen.routes) {
            ScreenAsignaturasStudent(userInfoState, studentviewModel, navController)
        }
        composable(
            route = "${StudentScreens.studentActividadScreen.routes}/{idAsignatura}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType })
        ) {
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            ActividadesEstudiantesScreen(studentviewModel, userInfoState, idAsignatura, navController)
        }
        composable(
            route = "${StudentScreens.studentEstadisticasScreen.routes}/{idAsignatura}",
            arguments = listOf(navArgument("idAsignatura") { type = NavType.StringType })
        ){
            val idAsignatura = it.arguments?.getString("idAsignatura")?.toIntOrNull()
            EstadisticasEs(studentviewModel, idAsignatura, navController)
        }
    }
}