package com.example.eduapp.untils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log

fun isOnline (context: Context): Boolean {
    val connectivityManager =  context.getSystemService (Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    // Si la versión de Android es igual o superior a la M
    val capabilities =  connectivityManager.getNetworkCapabilities (connectivityManager.activeNetwork)
    if (capabilities != null) {
        if (capabilities.hasTransport (NetworkCapabilities.TRANSPORT_CELLULAR)) {
            Log.e("network","internet for datos")
            return true
        } else if (capabilities.hasTransport (NetworkCapabilities.TRANSPORT_WIFI)) {
            Log.e("network","internet for wifi")
            return true
        } else if (capabilities.hasTransport (NetworkCapabilities.TRANSPORT_ETHERNET)) {
            Log.e("network","internet for ethernet")
            return true
        }
    }
    return false
}