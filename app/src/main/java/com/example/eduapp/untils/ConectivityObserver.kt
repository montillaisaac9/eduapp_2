package com.example.eduapp.untils

import kotlinx.coroutines.flow.Flow


interface ConectivityObserver {

    fun obsever(): Flow<Boolean>}