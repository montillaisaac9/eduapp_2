package com.example.eduapp.domain.auth.useCases

import android.util.Log
import com.example.eduapp.data.repository.RepositoryAuth
import com.example.eduapp.domain.model.NewProfesorDomain
import com.example.eduapp.domain.model.toRe
import javax.inject.Inject

class LogOutUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {
    suspend operator fun invoke(isProfesor: Boolean){

        if (isProfesor) repositoryAuth.logOutSessionProfesor()
        else repositoryAuth.logOutSessionStudent()
    }
}