package com.example.eduapp.domain.auth.useCases

import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.repository.RepositoryAuth
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetUserPrUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {
    operator fun invoke(): Flow<List<UserProfesorEntity>> {
        return repositoryAuth.getProfesorFromDatabaseWithFlow()
    }
}