package com.example.eduapp.domain.model

import com.example.eduapp.data.database.entities.ActividadEntity
import com.example.eduapp.data.remote.dataModelsRE.ActividadRe

data class ActividadDomain(
    val id: Int,
    val asignatura_id: Int,
    val nombre: String,
    val descripcion: String,
    val puntuacion_Max: Int,
    val fecha_limit: String,
) {

}


fun ActividadRe.toDomain() = ActividadDomain(id, asignatura_id, nombre, descripcion, puntuacion_Max, fecha_limit)


