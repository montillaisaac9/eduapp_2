package com.example.eduapp.domain.auth.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.remote.dataModelsRE.crear.NewProfesorRe
import com.example.eduapp.data.repository.RepositoryAuth
import com.example.eduapp.domain.model.NewProfesorDomain
import com.example.eduapp.domain.model.toDomain
import com.example.eduapp.domain.model.toRe
import javax.inject.Inject

class EditarPerfilPrUc @Inject constructor(private val repositoryAuth: RepositoryAuth){
    suspend operator fun invoke(id: Int, newProfesorDomain: NewProfesorDomain): Boolean{
        repositoryAuth.editarProfesor(id, newProfesorDomain.toRe())
        val profesor = repositoryAuth.getProfesor(id)
        if (profesor != null){
            repositoryAuth.clearCodeProfesor()
            val domain = profesor.map { it!!.toDomain() }
            repositoryAuth.insertUserProfesor(domain.map { it.toDatabase() })
            return true
            Log.d("se logro", "se modifico ${profesor} en la base de datos")
        } else {
            return false
        }
    }
}