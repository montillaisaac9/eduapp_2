package com.example.eduapp.domain.model

import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.example.eduapp.data.remote.dataModelsRE.crear.NewStudentRe


data class NewStudentDomain(
    val nombre: String,
    val correo: String,
    val contraseña: String,
    val fechaNacimiento: String?,
    val foto: String?,
    val sexo: String?,
    val telefono: String,
    val latitud: String?,
    val longitud: String?,
    val materias: List<MateriaInscita>?
)

fun NewStudentDomain.toRE()= NewStudentRe(nombre, correo, contraseña, fechaNacimiento, foto, sexo, telefono, latitud, longitud, materias)
