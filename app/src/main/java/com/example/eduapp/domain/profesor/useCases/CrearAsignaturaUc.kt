package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.NewAsignaturaDomain
import com.example.eduapp.domain.model.toRe
import javax.inject.Inject

class CrearAsignaturaUc @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {

    suspend operator fun invoke(newAsignaturasDomain: NewAsignaturaDomain):Boolean{
        return repositoryProfesor.register_Asignatura(newAsignaturasDomain.toRe())
    }
}