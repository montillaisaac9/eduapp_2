package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.database.entities.toDomain
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetActiviyByIdUC @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {
    suspend operator fun invoke(id: Int): Flow<ActividadDomain?> = flow {
        repositoryProfesor.getActividadByID(id).collect {
            if (it != null){
                val nota = it.toDomain()
                emit(nota)
            } else emit(null)
        }
    }
}