package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetAsignaturasUc @Inject constructor(private val repositoryStudent: RepositoryStudent) {

    suspend operator fun invoke(): Flow<List<AsignaturaDomain?>> = flow {
        val asignaturas = repositoryStudent.obtener_Asignaturas()
        if (asignaturas.isNotEmpty()) {
            repositoryStudent.clearCodeAsignature()
            val toDatabase = asignaturas.mapNotNull { it?.toDatabase() }
            repositoryStudent.insertsAsignature(toDatabase)
            repositoryStudent.getAsignatureDB().collect { asignaturasDB ->
                asignaturasDB?.let { it -> emit(it.map { it.toDomain() }) }
            }
        } else {
            emit(emptyList())
        }
    }


}