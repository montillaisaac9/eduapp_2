package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.repository.RepositoryStudent
import javax.inject.Inject

class VerifyCalificacion @Inject constructor(private val repositoryStudent: RepositoryStudent) {

    suspend operator fun invoke(id_student: String, asignaturaId: Int, id_actividad: Int): Boolean {
        val entregado = repositoryStudent.verificarSiEntrego(id_student, asignaturaId, id_actividad)
        return if (entregado.isEmpty()){
            false
        } else {
            true
        }
    }
}