package com.example.eduapp.domain.student.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ObtenerNotasMostrarUc @Inject constructor(private val repositoryStudent: RepositoryStudent) {
    suspend operator fun invoke(id_student: String, asignaturaId: Int, id_actividad: Int): Flow<List<NotaDomain?>> = flow {
        val nota = repositoryStudent.verificarSiEntrego(id_student, asignaturaId, id_actividad)
        if (nota.isNotEmpty()) {
            repositoryStudent.clearCodeNotas()
            val domain = nota.map { it!!.toDomain() }
            repositoryStudent.insertsNotas(domain.map { it.toDatabase() })
            repositoryStudent.getAllNotes().collect { notasDB ->
                Log.e("notas caso de uso", notasDB.toString())
                emit(notasDB.map { it.toDomain() })
            }
        } else {
            emit(emptyList())
        }
    }
}