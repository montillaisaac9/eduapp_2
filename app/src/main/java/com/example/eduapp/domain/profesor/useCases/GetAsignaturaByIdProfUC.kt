package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
class GetAsignaturaByIdProfUC @Inject constructor(private val repositoryProfesor: RepositoryProfesor){
    suspend operator fun invoke(id: Int): Flow<AsignaturaDomain> = flow {
        repositoryProfesor.getAsignaturaByID(id).collect {
            val asignatura = it?.toDomain() // Usar el operador de llamada segura
            if (asignatura != null) {
                emit(asignatura)
            }
        }
    }
}
