package com.example.eduapp.domain.auth

import com.example.eduapp.domain.auth.useCases.EditarPerfilEsUc
import com.example.eduapp.domain.auth.useCases.EditarPerfilPrUc
import com.example.eduapp.domain.auth.useCases.GetUserEsUseCase
import com.example.eduapp.domain.auth.useCases.GetUserPrUseCase
import com.example.eduapp.domain.auth.useCases.LogOutUseCase
import com.example.eduapp.domain.auth.useCases.LoginProfesorUsesCases
import com.example.eduapp.domain.auth.useCases.LoginStudentUsesCases
import com.example.eduapp.domain.auth.useCases.RegisterProfesorUseCase
import com.example.eduapp.domain.auth.useCases.RegisterStudentUseCase
import com.example.eduapp.domain.auth.useCases.VerifyProfesorUseCase
import com.example.eduapp.domain.auth.useCases.VerifyStudentUseCase

data class UseCaseAuth(
    val loginP: LoginProfesorUsesCases,
    val loginS: LoginStudentUsesCases,
    val registerP: RegisterProfesorUseCase,
    val registerS: RegisterStudentUseCase,
    val verifyS: VerifyStudentUseCase,
    val verifyP: VerifyProfesorUseCase,
    val getProfesor: GetUserPrUseCase,
    val getStudent: GetUserEsUseCase,
    val logOut: LogOutUseCase,
    val editProfesor: EditarPerfilPrUc,
    val editStudent: EditarPerfilEsUc
)
