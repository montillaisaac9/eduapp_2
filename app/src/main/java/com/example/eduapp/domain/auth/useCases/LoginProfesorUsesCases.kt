package com.example.eduapp.domain.auth.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryAuth
import com.example.eduapp.domain.model.ProfesorDomain
import javax.inject.Inject


class LoginProfesorUsesCases @Inject constructor(private val repositoryAuth: RepositoryAuth) {

        suspend operator fun invoke(email: String, password: String): List<ProfesorDomain>? {
            val profesor = repositoryAuth.loggin_p(email, password)

            return if (profesor!!.isNotEmpty()){
                repositoryAuth.clearCodeProfesor()
                repositoryAuth.insertUserProfesor(profesor.map { it.toDatabase() })
                Log.d("se logro", "se inserto ${profesor} en la base de datos")
                profesor
            } else {
                null
            }
        }
    }

