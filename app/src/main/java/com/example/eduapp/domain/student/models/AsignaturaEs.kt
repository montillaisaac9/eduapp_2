package com.example.eduapp.domain.student.models

import com.example.eduapp.data.database.entities.AsignaturaEntity
import com.example.eduapp.data.remote.dataModelsRE.AsignaturasRe
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito

data class AsignaturaPr (
    val id: Int,
    val nombre: String,
    val profesorId: Int,
    val descripcion: String,
    val estudiantes: List<EstudianteInscrito>?
)

fun AsignaturasRe.toDomain() = AsignaturaPr(id, nombre, profesorId, descripcion, estudiantes)
fun AsignaturaEntity.toDomain() = AsignaturaPr(id, nombre, profesorId, descripcion, estudiantes)