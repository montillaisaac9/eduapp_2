package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RefreshUserUc @Inject constructor(private val repositoryStudent: RepositoryStudent) {
    suspend operator fun invoke(id: String): Flow<List<UserStudentEntity>> {
        val student = repositoryStudent.getUser(id)
        repositoryStudent.clearCodeStudent()
        if (student != null) {
            repositoryStudent.insertUserStudent(student.toDomain().toDatabase())
        }
        return repositoryStudent.getStudentFromDatabaseWithFlow()

    }
}