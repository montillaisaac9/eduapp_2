package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DeleteStudentAsUc @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {

    suspend operator fun invoke(
        sR: InsStudentsRe,
        idEs: String,
        sA: InsAsignaturaRe,
        idAs: Int
    ){
        repositoryProfesor.eliminar(sR, idEs, sA, idAs)
    }
}