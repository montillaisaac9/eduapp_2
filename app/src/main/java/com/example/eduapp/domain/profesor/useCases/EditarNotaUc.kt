package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toRe
import javax.inject.Inject

class EditarNotaUc @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {
    suspend operator fun invoke(id: Int, notaE: NotaDomain){
        repositoryProfesor.notaEditar(id, notaE.toRe())
    }
}