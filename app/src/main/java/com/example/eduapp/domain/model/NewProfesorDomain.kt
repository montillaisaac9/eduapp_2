package com.example.eduapp.domain.model

import com.example.eduapp.data.remote.dataModelsRE.crear.NewProfesorRe

data class NewProfesorDomain(
    val nombre: String,
    val correo: String,
    val contraseña: String,
    val telefono: String
)

fun NewProfesorDomain.toRe() = NewProfesorRe( nombre, correo, contraseña, telefono)