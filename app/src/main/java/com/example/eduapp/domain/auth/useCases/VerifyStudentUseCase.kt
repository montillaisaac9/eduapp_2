package com.example.eduapp.domain.auth.useCases;

import com.example.eduapp.data.repository.RepositoryAuth
import javax.inject.Inject

public class VerifyStudentUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {

    suspend operator fun invoke(email: String): Boolean {

        val student = repositoryAuth.verify_s(email)

        return student?.isNotEmpty() == true
    }
}
