package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetStudentUc @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {
    suspend operator fun invoke(id: String): Flow<List<UserStudentEntity>> {
        val student = repositoryProfesor.getStudent(id)
        repositoryProfesor.clearCodeStudent()
        if (student != null) {
            repositoryProfesor.insertUserStudent(student.toDomain().toDatabase())
        }
        return repositoryProfesor.getStudentFromDatabaseWithFlow()

    }
}