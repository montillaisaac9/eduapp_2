package com.example.eduapp.domain.model

import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.remote.dataModelsRE.StudentRE
import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita


data class StudentDomain(
    val id: String,
    val nombre: String,
    val correo: String,
    val contraseña: String,
    val fechaNacimiento: String,
    val foto: String,
    val sexo: String,
    val telefono: String,
    val latitud: String,
    val longitud: String,
    val materias: List<MateriaInscita>,
    val edad: Int?
)

fun StudentRE.toDomain()= StudentDomain(id, nombre, correo, contraseña, fechaNacimiento, foto, sexo, telefono, latitud, longitud, materias, null)
fun UserStudentEntity.toDomain()= StudentDomain(id, nombre, correo, contraseña, fechaNacimiento, foto, sexo, telefono, latitud, longitud, materias, null)