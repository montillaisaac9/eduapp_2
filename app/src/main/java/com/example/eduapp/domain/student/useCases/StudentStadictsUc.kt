package com.example.eduapp.domain.student.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class StudentStadictsUc(private val repositoryStudent: RepositoryStudent) {
    suspend operator fun invoke(id_asignatura: Int): Flow<List<NotaDomain?>> = flow {
        val notas = repositoryStudent.obtenerNotasForStadicts(id_asignatura)
        if (notas.isNotEmpty()) {
            val notasFiltradas = notas.filter { it?.puntuacion != null }
            repositoryStudent.clearCodeNotas()
            val toDatabase = notasFiltradas.mapNotNull { it?.toDatabase() }
            repositoryStudent.insertsNotas(toDatabase)
            repositoryStudent.getAllNotes().collect { notasDB ->
                emit(notasDB.map { it.toDomain() })
            }
        } else {
            emit(emptyList())
        }
    }
}
