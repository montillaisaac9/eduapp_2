package com.example.eduapp.domain.auth.useCases


import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryAuth
import com.example.eduapp.domain.model.NewStudentDomain
import com.example.eduapp.domain.model.toDomain
import com.example.eduapp.domain.model.toRE
import javax.inject.Inject

class EditarPerfilEsUc @Inject constructor(private val repositoryAuth: RepositoryAuth) {
    suspend operator fun invoke(id: String, newStudentDomain: NewStudentDomain): Boolean{
        repositoryAuth.editarStudent(id, newStudentDomain.toRE())
        val profesor = repositoryAuth.getStudent(id)
        if (profesor != null){
            repositoryAuth.clearCodeProfesor()
            val domain = profesor.map { it!!.toDomain() }
            repositoryAuth.insertUserStudent(domain.map { it.toDatabase() })
            return true
            Log.d("se logro", "se modifico el estudiante${domain} en la base de datos")
        } else {
            return false
        }
    }
}