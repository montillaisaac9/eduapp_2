package com.example.eduapp.domain.model

import com.example.eduapp.data.remote.dataModelsRE.crear.NewActividadRe

data class NewActivityDomain(
    val asignatura_id: Int,
    val nombre: String,
    val descripcion: String,
    val puntuacion_Max: Int,
    val fecha_limit: String,
)

fun NewActivityDomain.toRe() = NewActividadRe(asignatura_id,nombre,descripcion,puntuacion_Max,fecha_limit)
