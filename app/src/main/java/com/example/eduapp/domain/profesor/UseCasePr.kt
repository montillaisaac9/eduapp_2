package com.example.eduapp.domain.profesor

import com.example.eduapp.domain.profesor.useCases.CrearActividadUC
import com.example.eduapp.domain.profesor.useCases.CrearAsignaturaUc
import com.example.eduapp.domain.profesor.useCases.DeleteStudentAsUc
import com.example.eduapp.domain.profesor.useCases.EditarNotaUc
import com.example.eduapp.domain.profesor.useCases.GetActividadByMateriaProfUc
import com.example.eduapp.domain.profesor.useCases.GetActiviyByIdUC
import com.example.eduapp.domain.profesor.useCases.GetAsignaturaByIdProfUC
import com.example.eduapp.domain.profesor.useCases.GetAsignaturaUC
import com.example.eduapp.domain.profesor.useCases.GetNotasToUpdate
import com.example.eduapp.domain.profesor.useCases.GetStudentUc
import com.example.eduapp.domain.profesor.useCases.ProfesorStadictsUc

data class UseCasePr(
    val createSignature: CrearAsignaturaUc,
    val getAsignature: GetAsignaturaUC,
    val gerAsignaturaById: GetAsignaturaByIdProfUC,
    val deletedEsAs: DeleteStudentAsUc,
    val getStudentById: GetStudentUc,
    val createActivity: CrearActividadUC,
    val getActividadByMateria: GetActividadByMateriaProfUc,
    val getNotasToUpdate: GetNotasToUpdate,
    val getActiviyById: GetActiviyByIdUC,
    val editarNota: EditarNotaUc,
    val stadictsUc: ProfesorStadictsUc
)
