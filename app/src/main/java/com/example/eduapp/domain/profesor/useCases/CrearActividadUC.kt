package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.NewActivityDomain
import com.example.eduapp.domain.model.NewAsignaturaDomain
import com.example.eduapp.domain.model.toRe
import javax.inject.Inject

class CrearActividadUC @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {

    suspend operator fun invoke(newActivityDomain: NewActivityDomain):Boolean{
        return repositoryProfesor.crearActividad(newActivityDomain.toRe())
    }
}