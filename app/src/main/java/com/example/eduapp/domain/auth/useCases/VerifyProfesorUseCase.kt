package com.example.eduapp.domain.auth.useCases;

import com.example.eduapp.data.repository.RepositoryAuth
import javax.inject.Inject

public class VerifyProfesorUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {

    suspend operator fun invoke(email: String): Boolean? {

        val profesor = repositoryAuth.verify_p(email)

        return profesor?.isNotEmpty() == true
    }
}