package com.example.eduapp.domain.auth.useCases

import com.example.eduapp.domain.model.NewProfesorDomain
import com.example.eduapp.domain.model.toRe
import com.example.eduapp.data.repository.RepositoryAuth
import javax.inject.Inject

class RegisterProfesorUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {


    suspend operator fun invoke(newProfesorDomain: NewProfesorDomain):Boolean{
        return repositoryAuth.register_p(newProfesorDomain.toRe())
    }
}