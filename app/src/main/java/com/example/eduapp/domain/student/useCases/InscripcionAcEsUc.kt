package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import com.example.eduapp.data.repository.RepositoryStudent
import javax.inject.Inject

class InscripcionAcEsUc @Inject constructor(private val repositoryStudent: RepositoryStudent) {

    suspend operator fun invoke(
        sR: InsStudentsRe,
        idEs: String,
        sA: InsAsignaturaRe,
        idAs: Int
    ){
        repositoryStudent.inscribir(sR, idEs, sA, idAs)
    }
}