package com.example.eduapp.domain.profesor.useCases

import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.database.entities.toDomain
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.ActividadDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetActividadByMateriaProfUc @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {

    suspend operator fun invoke(id: Int): Flow<List<ActividadDomain?>> = flow {
        val actividad = repositoryProfesor.obtenerActividadByAsignatura(id)
        if (actividad.isNotEmpty()) {
            repositoryProfesor.clearCodeActivity()
            val toDatabase = actividad.mapNotNull { it?.toDatabase() }
            repositoryProfesor.insertsActivity(toDatabase)
            repositoryProfesor.getActivitiesDB().collect { actividadDB ->
                emit(actividadDB.map { it.toDomain() })
            }
        } else {
            emit(emptyList())
        }
    }
}