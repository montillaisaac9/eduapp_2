package com.example.eduapp.domain.model

import com.example.eduapp.data.remote.dataModelsRE.crear.NewNotaRe

data class NewNotaDomain(

    val studentId : String,
    val asignaturaId: Int,
    val actividadId: Int,
    val puntuacion: Int?,
    val fecha: String

)

fun NewNotaDomain.toRe() = NewNotaRe(studentId, asignaturaId, actividadId, puntuacion, fecha)

