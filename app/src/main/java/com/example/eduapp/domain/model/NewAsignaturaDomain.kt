package com.example.eduapp.domain.model

import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.example.eduapp.data.remote.dataModelsRE.crear.NewAsignaturaRe


data class NewAsignaturaDomain(
    val nombre: String,
    val profesorId: Int,
    val descripcion: String,
    val estudiantesIds: List<EstudianteInscrito>,
    val token: String?
)

fun NewAsignaturaDomain.toRe() = NewAsignaturaRe( nombre, profesorId, descripcion, estudiantesIds, token)

