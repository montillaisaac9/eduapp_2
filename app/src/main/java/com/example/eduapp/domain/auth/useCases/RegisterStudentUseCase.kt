package com.example.eduapp.domain.auth.useCases

import com.example.eduapp.domain.model.NewStudentDomain
import com.example.eduapp.domain.model.toRE
import com.example.eduapp.data.repository.RepositoryAuth
import javax.inject.Inject

class RegisterStudentUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {
    suspend operator fun invoke(student: NewStudentDomain):Boolean{
        return repositoryAuth.register_s(student.toRE())
    }
}