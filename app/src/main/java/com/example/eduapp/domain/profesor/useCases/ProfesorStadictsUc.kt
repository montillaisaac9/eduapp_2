package com.example.eduapp.domain.profesor.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class ProfesorStadictsUc(private val repositoryProfesor: RepositoryProfesor) {
    suspend operator fun invoke(id_asignatura: Int): Flow<List<NotaDomain?>> = flow {
        val notas = repositoryProfesor.obtenerNotasForStadicts(id_asignatura)
        if (notas.isNotEmpty()) {
            val notasFiltradas = notas.filter { it?.puntuacion != null }
            repositoryProfesor.clearCodeNotas()
            val toDatabase = notasFiltradas.mapNotNull { it?.toDatabase() }
            repositoryProfesor.insertsNotas(toDatabase)
            repositoryProfesor.getAllNotes().collect { notasDB ->
                emit(notasDB.map { it.toDomain() })
            }
        } else {
            emit(emptyList())
        }
    }
}
