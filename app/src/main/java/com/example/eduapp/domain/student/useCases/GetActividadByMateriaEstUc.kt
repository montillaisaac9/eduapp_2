package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.database.entities.toDomain
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.ActividadDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetActividadByMateriaEstUc @Inject constructor(private val repositoryStudent: RepositoryStudent) {
    suspend operator fun invoke(id: Int): Flow<List<ActividadDomain?>> = flow {
        val actividad = repositoryStudent.obtenerActividadByAsignatura(id)
        if (actividad.isNotEmpty()) {
            repositoryStudent.clearCodeActivity()
            val toDatabase = actividad.mapNotNull { it?.toDatabase() }
            repositoryStudent.insertsActivity(toDatabase)
            repositoryStudent.getActivitiesDB().collect { actividadDB ->
                emit(actividadDB.map { it.toDomain() })
            }
        } else {
            emit(emptyList())
        }
    }
}