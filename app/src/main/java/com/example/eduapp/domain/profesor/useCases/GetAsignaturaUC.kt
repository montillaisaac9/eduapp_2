package com.example.eduapp.domain.profesor.useCases


import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetAsignaturaUC @Inject constructor(private val repositoryProfesor: RepositoryProfesor){

    suspend operator fun invoke(id: Int): Flow<List<AsignaturaDomain?>> = flow {
        val asignaturas = repositoryProfesor.obtener_Asignatura(id)
        if (asignaturas.isNotEmpty()) {
            repositoryProfesor.clearCodeAsignature()
            Log.e("Datos a insertar", asignaturas.toString())
            val toDatabase = asignaturas.mapNotNull { it?.toDatabase() }
            repositoryProfesor.insertsAsignature(toDatabase)
            repositoryProfesor.getAsignatureDB().collect { asignaturasDB ->
                asignaturasDB?.let { it -> emit(it.map { it.toDomain() }) }
            }
        } else {
            emit(emptyList())
        }
    }
}