package com.example.eduapp.domain.model

import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.remote.dataModelsRE.ProfesorRE

data class ProfesorDomain(
    val id: Int,
    val nombre: String,
    val correo: String,
    val contraseña: String,
    val telefono: String
)

fun ProfesorRE.toDomain() = ProfesorDomain(id, nombre, correo, contraseña, telefono)

fun UserProfesorEntity.toDomain() = ProfesorDomain(id, nombre, correo, contraseña, telefono)