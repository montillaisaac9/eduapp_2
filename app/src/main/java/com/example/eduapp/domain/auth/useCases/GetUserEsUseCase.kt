package com.example.eduapp.domain.auth.useCases

import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.repository.RepositoryAuth
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetUserEsUseCase @Inject constructor(private val repositoryAuth: RepositoryAuth) {
    operator fun invoke(): Flow<List<UserStudentEntity>> {
        return repositoryAuth.getStudentFromDatabaseWithFlow()
    }
}