package com.example.eduapp.domain.model

import com.example.eduapp.data.database.entities.NotaEntity
import com.example.eduapp.data.remote.dataModelsRE.NotaRe

data class NotaDomain(

    val id: Int,
    val studentId : String,
    val asignaturaId: Int,
    val actividadId: Int,
    val puntuacion: Int?,
    val fecha: String

)

fun NotaRe.toDomain() = NotaDomain(id,studentId,asignaturaId,actividadId,puntuacion, fecha)

fun NotaDomain.toRe()= NotaRe(id,studentId, asignaturaId, actividadId, puntuacion, fecha)
fun NotaEntity.toDomain() = NotaDomain(id,studentId, asignaturaId, actividadId, puntuacion, fecha)
