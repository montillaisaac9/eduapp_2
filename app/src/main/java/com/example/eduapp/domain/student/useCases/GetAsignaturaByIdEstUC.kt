package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
class GetAsignaturaByIdEstUC @Inject constructor(private val repositoryStudent: RepositoryStudent){
    suspend operator fun invoke(id: Int): Flow<AsignaturaDomain> = flow {
        repositoryStudent.getAsignaturaByID(id).collect {
            if (it != null) {
                emit(it.toDomain())
            }
        }
    }
}
