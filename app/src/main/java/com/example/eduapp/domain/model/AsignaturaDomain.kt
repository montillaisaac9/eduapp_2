package com.example.eduapp.domain.model

import com.example.eduapp.data.database.entities.AsignaturaEntity
import com.example.eduapp.data.remote.dataModelsRE.AsignaturasRe
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito

data class AsignaturaDomain (
    val id: Int,
    val nombre: String,
    val profesorId: Int,
    val descripcion: String,
    val estudiantes: List<EstudianteInscrito>?,
    val token: String?
)

fun AsignaturasRe.toDomain() = AsignaturaDomain(id, nombre, profesorId, descripcion, estudiantes, token_pr)
fun AsignaturaEntity.toDomain() = AsignaturaDomain(id, nombre, profesorId, descripcion, estudiantes, token)