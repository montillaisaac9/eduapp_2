package com.example.eduapp.domain.student.useCases

import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.model.NewAsignaturaDomain
import com.example.eduapp.domain.model.NewNotaDomain
import com.example.eduapp.domain.model.toRe
import javax.inject.Inject

class CrearCalificacion @Inject constructor(private val repositoryStudent: RepositoryStudent) {

    suspend operator fun invoke(newCalificacion: NewNotaDomain):Boolean{
        return repositoryStudent.crearCalificacion(newCalificacion.toRe())
    }
}