package com.example.eduapp.domain.auth.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryAuth

import com.example.eduapp.domain.model.StudentDomain
import javax.inject.Inject

class LoginStudentUsesCases @Inject constructor(private val repositoryAuth: RepositoryAuth) {

    suspend operator fun invoke(email: String, password: String): List<StudentDomain>? {
        // Realizar la solicitud de inicio de sesión a la API
        val student = repositoryAuth.loggin_s(email, password)

        return if (student?.isNotEmpty() == true) {
            // Si la lista no está vacía, borrar cualquier dato existente y guardar los nuevos en la base de datos
            repositoryAuth.clearCodeStudent()
            repositoryAuth.insertUserStudent(student.map { it.toDatabase() })
            Log.d("se logro", "se inserto ${student} en la base de datos")
            student
        } else {
            null
        }
    }

}

