package com.example.eduapp.domain.profesor.useCases

import android.util.Log
import com.example.eduapp.data.database.entities.toDatabase
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetNotasToUpdate @Inject constructor(private val repositoryProfesor: RepositoryProfesor) {
    suspend operator fun invoke(id_actividad: Int, id_asignatura: Int): Flow<List<NotaDomain?>> = flow {
        val notas = repositoryProfesor.obtenerNotasByAcitvidadId(id_actividad, id_asignatura)
        if (notas.isNotEmpty()) {
            repositoryProfesor.clearCodeNotas()
            val toDatabase = notas.map { it!!.toDatabase() }
            repositoryProfesor.insertsNotas(toDatabase)
            repositoryProfesor.getAllNotes().collect { notasDB ->
                Log.e("notas para editar", notasDB.toString())
                emit(notasDB.map { it.toDomain() })
            }
        } else {
            emit(emptyList())
        }
    }
}