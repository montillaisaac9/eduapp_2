package com.example.eduapp.domain.student

import com.example.eduapp.domain.student.useCases.CrearCalificacion
import com.example.eduapp.domain.student.useCases.GetActividadByMateriaEstUc
import com.example.eduapp.domain.student.useCases.GetAsignaturaByIdEstUC
import com.example.eduapp.domain.student.useCases.GetAsignaturasUc
import com.example.eduapp.domain.student.useCases.InscripcionAcEsUc
import com.example.eduapp.domain.student.useCases.ObtenerNotasMostrarUc
import com.example.eduapp.domain.student.useCases.RefreshUserUc
import com.example.eduapp.domain.student.useCases.StudentStadictsUc
import com.example.eduapp.domain.student.useCases.VerifyCalificacion

data class UseCaseEs (
    val getAllasignatura: GetAsignaturasUc,
    val inscripcion: InscripcionAcEsUc,
    val refrescar: RefreshUserUc,
    val getActividadByMateria: GetActividadByMateriaEstUc,
    val getAsignaturaById: GetAsignaturaByIdEstUC,
    val crearNota: CrearCalificacion,
    val verificarNota: VerifyCalificacion,
    val getStadictsUc: StudentStadictsUc,
    val obtenerNotas: ObtenerNotasMostrarUc
)