package com.example.eduapp.di

import android.content.Context
import androidx.room.Room
import com.example.eduapp.data.database.DataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    private val database_name = "base_de_Datos"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) = Room.databaseBuilder(
        context, DataBase::class.java, database_name).build()

    @Singleton
    @Provides
    fun provideDao(db: DataBase) = db.getUserDao()
}