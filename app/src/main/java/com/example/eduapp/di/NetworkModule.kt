package com.example.eduapp.di

import com.example.eduapp.domain.auth.useCases.LoginProfesorUsesCases
import com.example.eduapp.domain.auth.useCases.LoginStudentUsesCases
import com.example.eduapp.domain.auth.useCases.RegisterProfesorUseCase
import com.example.eduapp.domain.auth.useCases.RegisterStudentUseCase
import com.example.eduapp.domain.auth.UseCaseAuth
import com.example.eduapp.domain.auth.useCases.VerifyProfesorUseCase
import com.example.eduapp.domain.auth.useCases.VerifyStudentUseCase
import com.example.eduapp.data.remote.AuthRepository
import com.example.eduapp.data.remote.ProfesorRepository
import com.example.eduapp.data.remote.StudentRepository
import com.example.eduapp.data.repository.RepositoryAuth
import com.example.eduapp.data.repository.RepositoryProfesor
import com.example.eduapp.data.repository.RepositoryStudent
import com.example.eduapp.domain.auth.useCases.EditarPerfilEsUc
import com.example.eduapp.domain.auth.useCases.EditarPerfilPrUc
import com.example.eduapp.domain.auth.useCases.GetUserEsUseCase
import com.example.eduapp.domain.auth.useCases.GetUserPrUseCase
import com.example.eduapp.domain.auth.useCases.LogOutUseCase
import com.example.eduapp.domain.profesor.UseCasePr
import com.example.eduapp.domain.profesor.useCases.CrearActividadUC
import com.example.eduapp.domain.profesor.useCases.CrearAsignaturaUc
import com.example.eduapp.domain.profesor.useCases.DeleteStudentAsUc
import com.example.eduapp.domain.profesor.useCases.EditarNotaUc
import com.example.eduapp.domain.profesor.useCases.GetActividadByMateriaProfUc
import com.example.eduapp.domain.profesor.useCases.GetActiviyByIdUC
import com.example.eduapp.domain.profesor.useCases.GetAsignaturaByIdProfUC
import com.example.eduapp.domain.profesor.useCases.GetAsignaturaUC
import com.example.eduapp.domain.profesor.useCases.GetNotasToUpdate
import com.example.eduapp.domain.profesor.useCases.GetStudentUc
import com.example.eduapp.domain.profesor.useCases.ProfesorStadictsUc
import com.example.eduapp.domain.student.UseCaseEs
import com.example.eduapp.domain.student.useCases.CrearCalificacion
import com.example.eduapp.domain.student.useCases.GetActividadByMateriaEstUc
import com.example.eduapp.domain.student.useCases.GetAsignaturaByIdEstUC
import com.example.eduapp.domain.student.useCases.GetAsignaturasUc
import com.example.eduapp.domain.student.useCases.InscripcionAcEsUc
import com.example.eduapp.domain.student.useCases.ObtenerNotasMostrarUc
import com.example.eduapp.domain.student.useCases.RefreshUserUc
import com.example.eduapp.domain.student.useCases.StudentStadictsUc
import com.example.eduapp.domain.student.useCases.VerifyCalificacion
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit():Retrofit{
        return Retrofit.Builder()
            .baseUrl("http://apirest2.tecnoparaguana.org.ve/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideApiClient(retrofit: Retrofit): AuthRepository {
        return retrofit.create(AuthRepository::class.java)
    }

    @Singleton
    @Provides
    fun provideProfesorAclient(retrofit: Retrofit): ProfesorRepository {
        return retrofit.create(ProfesorRepository::class.java)
    }

    @Singleton
    @Provides
    fun provideStudentAclient(retrofit: Retrofit): StudentRepository {
        return retrofit.create(StudentRepository::class.java)
    }



    @Singleton
    @Provides
    fun provideUsesCases(repositoryAuth: RepositoryAuth)= UseCaseAuth(
        loginP = LoginProfesorUsesCases(repositoryAuth),
        loginS = LoginStudentUsesCases(repositoryAuth),
        registerP = RegisterProfesorUseCase(repositoryAuth),
        registerS = RegisterStudentUseCase(repositoryAuth),
        verifyP = VerifyProfesorUseCase(repositoryAuth),
        verifyS = VerifyStudentUseCase(repositoryAuth),
        getStudent = GetUserEsUseCase(repositoryAuth),
        getProfesor = GetUserPrUseCase(repositoryAuth),
        logOut = LogOutUseCase(repositoryAuth),
        editProfesor = EditarPerfilPrUc(repositoryAuth),
        editStudent = EditarPerfilEsUc(repositoryAuth)
    )

    @Singleton
    @Provides
    fun provideUsesCasesProfesor(repositoryProfesor: RepositoryProfesor)= UseCasePr(
        createSignature = CrearAsignaturaUc(repositoryProfesor),
        getAsignature = GetAsignaturaUC(repositoryProfesor),
        gerAsignaturaById = GetAsignaturaByIdProfUC(repositoryProfesor),
        deletedEsAs = DeleteStudentAsUc(repositoryProfesor),
        getStudentById = GetStudentUc(repositoryProfesor),
        createActivity = CrearActividadUC(repositoryProfesor),
        getActividadByMateria = GetActividadByMateriaProfUc(repositoryProfesor),
        getNotasToUpdate = GetNotasToUpdate(repositoryProfesor),
        getActiviyById = GetActiviyByIdUC(repositoryProfesor),
        editarNota = EditarNotaUc(repositoryProfesor),
        stadictsUc = ProfesorStadictsUc(repositoryProfesor)
    )

    @Singleton
    @Provides
    fun provideUsesCasesStudent(repositoryStudent: RepositoryStudent)= UseCaseEs(
        getAllasignatura = GetAsignaturasUc(repositoryStudent),
        inscripcion = InscripcionAcEsUc(repositoryStudent),
        refrescar = RefreshUserUc(repositoryStudent),
        getActividadByMateria = GetActividadByMateriaEstUc(repositoryStudent),
        getAsignaturaById = GetAsignaturaByIdEstUC(repositoryStudent),
        crearNota = CrearCalificacion(repositoryStudent),
        verificarNota = VerifyCalificacion(repositoryStudent),
        getStadictsUc = StudentStadictsUc(repositoryStudent),
        obtenerNotas = ObtenerNotasMostrarUc(repositoryStudent)
    )

}