package com.example.eduapp.data.remote.dataModelsRE.crear

import com.google.gson.annotations.SerializedName

data class NewNotaRe (
    @SerializedName("estudiante_id") val studentId : String,
    @SerializedName("asignatura_id") val asignaturaId: Int,
    @SerializedName("actividad_id") val actividadId: Int,
    @SerializedName("puntuacion") val puntuacion: Int?,
    @SerializedName("fecha_calificacion") val fecha: String
)