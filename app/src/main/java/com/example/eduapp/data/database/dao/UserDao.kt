package com.example.eduapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.eduapp.data.database.entities.ActividadEntity
import com.example.eduapp.data.database.entities.AsignaturaEntity
import com.example.eduapp.data.database.entities.NotaEntity
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.database.entities.UserStudentEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {
    //TODO insertar
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllUserEs(user: List<UserStudentEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllUserPr(user: List<UserProfesorEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAsignature(user: List<AsignaturaEntity>)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllActivitys(user: List<ActividadEntity>)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllNotas(user: List<NotaEntity>)

    //TODO QUERYS_ALL

    @Query("select * FROM user_student")
    fun getStudentUserWithFlow(): Flow<List<UserStudentEntity>>

    @Query("select * FROM user_profesor")
    fun getProfesorUserWithFlow(): Flow<List<UserProfesorEntity>>

    @Query("select * FROM Asignaturas")
    fun getAsignatureUserWithFlow(): Flow<List<AsignaturaEntity>?>
    @Query("select * FROM Activitys")
    fun getActivitysUserWithFlow(): Flow<List<ActividadEntity>>
    @Query("select * FROM Notas")
    fun getNotasWithFlow(): Flow<List<NotaEntity>>
    @Query("SELECT * FROM Asignaturas WHERE id = :userId")
    fun getUserAsignaturaById(userId: Int): Flow<AsignaturaEntity>

    @Query("SELECT * FROM Activitys WHERE id = :userId")
    fun getUserActividadById(userId: Int): Flow<ActividadEntity>

    //TODO RESETEAR TABLA
    @Query("DELETE FROM user_student")
    suspend fun deleteStudentUser()

    @Query("DELETE FROM user_profesor")
    suspend fun deleteProfesorUser()

    @Query("DELETE FROM Asignaturas")
    suspend fun deleteAsignature()

    @Query("DELETE FROM Activitys")
    suspend fun deleteActivitiys()
    @Query("DELETE FROM Notas")
    suspend fun deleteAllNotes()

}