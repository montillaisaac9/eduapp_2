package com.example.eduapp.data.remote

import com.example.eduapp.data.remote.dataModelsRE.ActividadRe
import com.example.eduapp.data.remote.dataModelsRE.AsignaturasRe
import com.example.eduapp.data.remote.dataModelsRE.NotaRe
import com.example.eduapp.data.remote.dataModelsRE.StudentRE
import com.example.eduapp.data.remote.dataModelsRE.crear.NewNotaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface StudentRepository {
    @GET("asignaturas")
    suspend fun asignaturasInscripcion(): Response<List<AsignaturasRe>?>
    @GET("estudiantes/{id}")
    suspend fun getStudentById(@Path("id") id: String): Response<StudentRE>
    @PATCH("estudiantes/{id}")
    suspend fun inscribirEstudiante(@Path("id") id: String, @Body usuario: InsStudentsRe): Response<Any>
    @PATCH("asignaturas/{id}")
    suspend fun inscribirAsignatura(@Path("id") id: Int, @Body usuario: InsAsignaturaRe): Response<Any>
    @GET("actividades")
    suspend fun obtner_actividadesByMateria(@Query("asignatura_id") id: Int): Response<List<ActividadRe>?>
    @GET("calificaciones")
    suspend fun verificar_nota(
        @Query("estudiante_id") id: String,
        @Query("asignatura_id")asignaturaId: Int,
        @Query("actividad_id")id_actividad: Int
    ): Response<List<NotaRe>?>

    @GET("calificaciones")
    suspend fun getCalificacionesForStadicts(
        @Query("asignatura_id") id_asignatura: Int
    ): Response<List<NotaRe>?>

    @POST("calificaciones")
    suspend fun crear_nota(@Body nota: NewNotaRe): Response<Any>


}