package com.example.eduapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.example.eduapp.domain.model.AsignaturaDomain

@Entity(tableName = "Asignaturas")
data class AsignaturaEntity(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "nombre")
    val nombre: String,

    @ColumnInfo(name = "profesor_id")
    val profesorId: Int,

    @ColumnInfo(name = "descipcion")
    val descripcion: String,

    @ColumnInfo(name = "Lista_Estudiantes")
    val estudiantes: List<EstudianteInscrito>?,

    @ColumnInfo(name = "token_pr")
    val token: String?
)

fun AsignaturaDomain.toDatabase() = AsignaturaEntity(id, nombre, profesorId, descripcion, estudiantes, token)