package com.example.eduapp.data.database

import androidx.room.TypeConverter
import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    @TypeConverter
    fun subjectsFromString(value: String?): List<MateriaInscita>? {
        return value?.let {
            Gson().fromJson(it, object : TypeToken<List<MateriaInscita>>() {}.type)
        }
    }

    @TypeConverter
    fun subjectsFromList(list: List<MateriaInscita>?): String? {
        return list?.let {
            Gson().toJson(it)
        }
    }

    @TypeConverter
    fun fromString(value: String?): List<EstudianteInscrito>? {
        return value?.let {
            Gson().fromJson(it, object : TypeToken<List<EstudianteInscrito>>() {}.type)
        }
    }

    @TypeConverter
    fun fromList(list: List<EstudianteInscrito>?): String? {
        return list?.let {
            Gson().toJson(it)
        }
    }

}
