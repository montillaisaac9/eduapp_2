package com.example.eduapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.ProfesorDomain

@Entity(tableName = "Notas")
data class NotaEntity (

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo("student_id")
    val studentId : String,

    @ColumnInfo("asignatura_id")
    val asignaturaId: Int,

    @ColumnInfo("actividad_id")
    val actividadId: Int,

    @ColumnInfo("puntuacion")
    val puntuacion: Int?,

    @ColumnInfo("fecha")
    val fecha: String
)

fun NotaDomain.toDatabase () = NotaEntity(id,studentId, asignaturaId, actividadId, puntuacion, fecha)