package com.example.eduapp.data.remote

import com.example.eduapp.data.remote.dataModelsRE.ActividadRe
import com.example.eduapp.data.remote.dataModelsRE.AsignaturasRe
import com.example.eduapp.data.remote.dataModelsRE.NotaRe
import com.example.eduapp.data.remote.dataModelsRE.StudentRE
import com.example.eduapp.data.remote.dataModelsRE.crear.NewActividadRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import com.example.eduapp.data.remote.dataModelsRE.crear.NewAsignaturaRe
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ProfesorRepository {

    //TODO FUNCIONALIDADES PROFESORES
    @POST("asignaturas")
    suspend fun crear_a(@Body asignatura: NewAsignaturaRe): Response<*>
    @POST("actividades")
    suspend fun crear_actividad(@Body actividad: NewActividadRe): Response<*>
    @GET("actividades")
    suspend fun obtner_actividadesByMateria(@Query("asignatura_id") id: Int): Response<List<ActividadRe>?>
    @GET("asignaturas")
    suspend fun asignaturas(
        @Query("profesor_id") id: Int
    ): Response<List<AsignaturasRe>?>
    @GET("estudiantes/{id}")
    suspend fun getStudentById(@Path("id") id: String): Response<StudentRE>

    @GET("calificaciones")
    suspend fun getCalificaciones(
        @Query("actividad_id") id_actividad: Int,
        @Query("asignatura_id") id_asignatura: Int
    ): Response<List<NotaRe>?>

    @GET("calificaciones")
    suspend fun getCalificacionesForStadicts(
        @Query("asignatura_id") id_asignatura: Int
    ): Response<List<NotaRe>?>

    @PATCH("estudiantes/{id}")
    suspend fun eliminarAsignatura(@Path("id") id: String, @Body usuario: InsStudentsRe): Response<Any>

    @PATCH("asignaturas/{id}")
    suspend fun eliminarEstudiante(@Path("id") id: Int, @Body usuario: InsAsignaturaRe): Response<Any>

    @PATCH("calificaciones/{id}")
    suspend fun editar_nota(@Path("id")id: Int, @Body nota: NotaRe): Response<Any>

}