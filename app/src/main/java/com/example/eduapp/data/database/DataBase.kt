package com.example.eduapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.eduapp.data.database.dao.UserDao
import com.example.eduapp.data.database.entities.ActividadEntity
import com.example.eduapp.data.database.entities.AsignaturaEntity
import com.example.eduapp.data.database.entities.NotaEntity
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.database.entities.UserStudentEntity

@Database(entities = [UserStudentEntity::class, UserProfesorEntity::class, AsignaturaEntity::class, ActividadEntity::class, NotaEntity::class], version = 1 )
@TypeConverters(Converters::class)
abstract class DataBase: RoomDatabase() {
   abstract fun getUserDao(): UserDao
}