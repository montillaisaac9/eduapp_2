package com.example.eduapp.data.remote.dataModelsRE.crear

import com.google.gson.annotations.SerializedName

data class NewActividadRe(
    @SerializedName("asignatura_id") val asignatura_id: Int,
    @SerializedName("nombre") val nombre: String,
    @SerializedName("descripcion") val descripcion: String,
    @SerializedName("puntuacion_maxima")val puntuacion_Max: Int,
    @SerializedName("fecha_limite")val fecha_limit: String,
)
