package com.example.eduapp.data.repository

import android.util.Log
import com.example.eduapp.data.database.dao.UserDao
import com.example.eduapp.data.database.entities.UserProfesorEntity
import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.remote.dataModelsRE.StudentRE
import com.example.eduapp.domain.model.ProfesorDomain
import com.example.eduapp.domain.model.StudentDomain
import com.example.eduapp.domain.model.toDomain
import com.example.eduapp.data.remote.AuthRepository
import com.example.eduapp.data.remote.dataModelsRE.NotaRe
import com.example.eduapp.data.remote.dataModelsRE.ProfesorRE
import com.example.eduapp.data.remote.dataModelsRE.crear.NewProfesorRe
import com.example.eduapp.data.remote.dataModelsRE.crear.NewStudentRe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import java.io.IOException
import java.lang.Exception
import javax.inject.Inject

class RepositoryAuth @Inject constructor(
    private val api: AuthRepository,
    private val userDao: UserDao
) {

    //Login
    suspend fun loggin_s(email: String, password: String): List<StudentDomain>? {
        return try {

            withContext(Dispatchers.IO){
                val response: retrofit2.Response<kotlin.collections.List<StudentRE>?> = api.loggin_s(email, password)
                val re = response.body() ?: emptyList()
                Log.d("datos obtenidos de retrofit", re.toString())
                re.map { it.toDomain() }
            }

        } catch (e: Exception) {
            Log.e("error", "${e}")
            return emptyList()
        }
    }

    suspend fun loggin_p(email: String, password: String): List<ProfesorDomain>? {
        return try {
            withContext(Dispatchers.IO){
                val response = api.loggin_p(email, password)
                val re = response.body() ?: emptyList()
                re.map { it.toDomain() }
            }
        } catch (e: Exception) {
            Log.e("error", "${e}")
            return emptyList()
        }
    }

    //verify
    suspend fun verify_s(email: String): List<StudentDomain>? {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.verify_s(email)
                val re = response.body() ?: emptyList()
                re.map { it.toDomain() }
            }
        } catch (e: Exception) {
            Log.e("error", "${e}")
            return emptyList()
        }
    }

    suspend fun verify_p(email: String): List<ProfesorDomain>? {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.verify_p(email)
                val re = response.body() ?: emptyList()
                re.map { it.toDomain() }
            }
        } catch (e: Exception) {
            Log.e("error", "${e}")
            return emptyList()
        }
    }

    //Register
    suspend fun register_s(studentRE: NewStudentRe):Boolean{
        return try {
            withContext(Dispatchers.IO) {
                val response = api.register_s(studentRE)
                true
            }
        } catch (e: Exception){
            Log.e("error","${e}")
            return false
        }
    }

    suspend fun register_p(profesorRE: NewProfesorRe):Boolean{
        return try {
            withContext(Dispatchers.IO) {
                val response = api.register_p(profesorRE)
                true
            }
        } catch (e: Exception){
            Log.e("error","${e}")
            return false
        }
    }

    suspend fun editarProfesor(id: Int, Pr: NewProfesorRe):Boolean {
        return try {
            withContext(Dispatchers.IO) {

                val res = api.editar_p(id, Pr)

                if (res.isSuccessful){
                    Log.d("se logro", "se logro")
                    true
                } else {
                    Log.d("error", res.message())
                    false
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            false
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            false
        }
    }

    suspend fun editarStudent(id: String, Es: NewStudentRe):Boolean {
        return try {
            withContext(Dispatchers.IO) {

                val res = api.editar_s(id, Es)

                if (res.isSuccessful){
                    Log.d("se logro", "se logro")
                    true
                } else {
                    Log.d("error", res.message())
                    false
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            false
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            false
        }
    }

    suspend fun getStudent(id: String): List<StudentRE?> {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.getStudentById(id)
                val res: StudentRE?
                if (response.isSuccessful) {
                    Log.d("se logro", "se logro")
                    res = response.body()
                    Log.d("repuesta", res.toString())
                    listOf(res)
                } else {
                    Log.d("error", response.message())
                    emptyList()
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            emptyList()
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            emptyList()
        }
    }

    suspend fun getProfesor(id: Int): List<ProfesorRE?> {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.getProfesorById(id)
                val res: ProfesorRE?
                if (response.isSuccessful) {
                    Log.d("se logro", "se logro")
                    res = response.body()
                    Log.d("repuesta", res.toString())
                    listOf(res)
                } else {
                    Log.d("error", response.message())
                    emptyList()
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            emptyList()
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            emptyList()
        }
    }

    //TODO Database
    suspend fun insertUserStudent(user: List<UserStudentEntity>){
        userDao.insertAllUserEs(user)
    }

    suspend fun insertUserProfesor(user: List<UserProfesorEntity>){
        userDao.insertAllUserPr(user)
    }
    fun getStudentFromDatabaseWithFlow(): Flow<List<UserStudentEntity>> {
        return userDao.getStudentUserWithFlow()
    }
    fun getProfesorFromDatabaseWithFlow(): Flow<List<UserProfesorEntity>> {
        return userDao.getProfesorUserWithFlow()
    }
    suspend fun clearCodeStudent(){
        userDao.deleteStudentUser()
    }

    suspend fun clearCodeProfesor(){
        userDao.deleteProfesorUser()
    }

    suspend fun logOutSessionProfesor() {
        userDao.deleteStudentUser()
        userDao.deleteActivitiys()
        userDao.deleteAsignature()
        userDao.deleteAllNotes()
        userDao.deleteProfesorUser()
    }

    suspend fun logOutSessionStudent() {
        userDao.deleteActivitiys()
        userDao.deleteAsignature()
        userDao.deleteAllNotes()
        userDao.deleteProfesorUser()
        userDao.deleteStudentUser()
    }
}

