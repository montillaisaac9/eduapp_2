package com.example.eduapp.data.remote


import com.example.eduapp.data.remote.dataModelsRE.AsignaturasRe
import com.example.eduapp.data.remote.dataModelsRE.ProfesorRE
import com.example.eduapp.data.remote.dataModelsRE.StudentRE
import com.example.eduapp.data.remote.dataModelsRE.crear.NewProfesorRe
import com.example.eduapp.data.remote.dataModelsRE.crear.NewStudentRe
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface AuthRepository {

    //TODO obtener
    @GET("estudiantes")
    suspend fun loggin_s(
        @Query("correo") email: String,
        @Query("contraseña") password: String
    ): Response<List<StudentRE>?>

    @GET("profesores")
    suspend fun loggin_p(
        @Query("correo") email: String,
        @Query("contraseña") password: String
    ): Response<List<ProfesorRE>?>

    @GET("estudiantes")
    suspend fun verify_s(
        @Query("correo") email: String
    ): Response<List<StudentRE>?>

    @GET("profesores")
    suspend fun verify_p(
        @Query("correo") email: String
    ): Response<List<ProfesorRE>?>

    @GET("estudiantes/{id}")
    suspend fun getStudentById(@Path("id") id: String): Response<StudentRE>

    @GET("profesores/{id}")
    suspend fun getProfesorById(@Path("id") id: Int): Response<ProfesorRE>


    //TODO registrar
    @POST("estudiantes")
    suspend fun register_s(@Body usuario: NewStudentRe): Response<*>


    @POST("profesores")
    suspend fun register_p(@Body usuario: NewProfesorRe): Response<*>

    @PATCH("profesores/{id}")
    suspend fun editar_p(@Path("id") id: Int,@Body usuario: NewProfesorRe): Response<*>

    @PATCH("estudiantes/{id}")
    suspend fun editar_s(@Path("id") id: String, @Body usuario: NewStudentRe): Response<*>
}
