package com.example.eduapp.data.remote.dataModelsRE.crear

import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.google.gson.annotations.SerializedName

data class NewStudentRe(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("correo") val correo: String?,
    @SerializedName("contraseña") val contraseña: String?,
    @SerializedName("fecha_de_nacimiento") val fechaNacimiento: String?,
    @SerializedName("foto") val foto: String?,
    @SerializedName("sexo") val sexo: String?,
    @SerializedName("telefono") val telefono: String,
    @SerializedName("latitud") val latitud: String?,
    @SerializedName("longitud") val longitud: String?,
    @SerializedName("materias_inscritas") val materias: List<MateriaInscita>?
)
