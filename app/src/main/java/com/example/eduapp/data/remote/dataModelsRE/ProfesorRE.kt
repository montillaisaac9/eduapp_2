package com.example.eduapp.data.remote.dataModelsRE
import com.google.gson.annotations.SerializedName

data class ProfesorRE(
    @SerializedName("id") val id: Int,
    @SerializedName("nombre") val nombre: String,
    @SerializedName("correo") val correo: String,
    @SerializedName("contraseña") val contraseña: String,
    @SerializedName("telefono") val telefono: String
)