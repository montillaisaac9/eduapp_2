package com.example.eduapp.data.repository

import android.util.Log
import com.example.eduapp.data.database.dao.UserDao
import com.example.eduapp.data.database.entities.ActividadEntity
import com.example.eduapp.data.database.entities.AsignaturaEntity
import com.example.eduapp.data.database.entities.NotaEntity
import com.example.eduapp.data.database.entities.UserStudentEntity
import com.example.eduapp.data.remote.ProfesorRepository
import com.example.eduapp.data.remote.dataModelsRE.NotaRe
import com.example.eduapp.data.remote.dataModelsRE.StudentRE
import com.example.eduapp.data.remote.dataModelsRE.crear.NewActividadRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsAsignaturaRe
import com.example.eduapp.data.remote.dataModelsRE.patch.InsStudentsRe
import com.example.eduapp.data.remote.dataModelsRE.crear.NewAsignaturaRe
import com.example.eduapp.domain.model.ActividadDomain
import com.example.eduapp.domain.model.AsignaturaDomain
import com.example.eduapp.domain.model.NotaDomain
import com.example.eduapp.domain.model.toDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow

import kotlinx.coroutines.withContext
import java.io.IOException
import java.lang.Exception
import javax.inject.Inject

class RepositoryProfesor @Inject constructor(
    private val api: ProfesorRepository,
    private val userDao: UserDao
)  {

    suspend fun register_Asignatura(newAsignaturaRe: NewAsignaturaRe):Boolean{
        return try {
            withContext(Dispatchers.IO) {
                val response = api.crear_a(newAsignaturaRe)
                true
            }
        } catch (e: Exception){
            Log.e("error","${e}")
            return false
        }
    }

    suspend fun obtener_Asignatura(id: Int): List<AsignaturaDomain?> {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.asignaturas(id)
                if (response.isSuccessful) {
                    val re = response.body() ?: emptyList()
                    re.map { it.toDomain() }
                } else {
                    emptyList()
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            // Manejar errores de red, tal vez mostrar un mensaje de error al usuario o intentar nuevamente
            emptyList()
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            // Manejar otros tipos de excepciones de manera genérica
            emptyList()
        }
    }

    suspend fun getStudent(id: String): StudentRE? {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.getStudentById(id)
                val res: StudentRE?
                if (response.isSuccessful) {
                    Log.d("se logro", "se logro")
                    res = response.body()
                    Log.d("repuesta", res.toString())
                    res
                } else {
                    Log.d("error", response.message())
                    null
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            null
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            null
        }
    }
    suspend fun eliminar(sR: InsStudentsRe, idEs: String, sR1: InsAsignaturaRe, idAs: Int):Boolean {
        return try {
            withContext(Dispatchers.IO) {
                val res = api.eliminarAsignatura(idEs, sR)
                val res2 = api.eliminarEstudiante(idAs, sR1)
                if (res.isSuccessful and  res2.isSuccessful){
                    Log.d("se logro", "se logro")
                    true
                } else {
                    Log.d("error", res.message())
                    false
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            false
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            false
        }
    }

    suspend fun crearActividad (newActividadRe: NewActividadRe): Boolean {
        return try {
            withContext(Dispatchers.IO) {
                val response = api.crear_actividad(newActividadRe)
                true
            }
        } catch (e: Exception){
            Log.e("error","${e}")
            return false
        }
    }

    suspend fun obtenerActividadByAsignatura(id: Int): List<ActividadDomain?> {
        return try {
            val response = api.obtner_actividadesByMateria(id)
            if (response.isSuccessful) {
                val re = response.body() ?: emptyList()
                re.map { it.toDomain() }
            } else {
                Log.e("ErrorResponse", "Error en la solicitud: ${response.code()}")
                emptyList()
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            emptyList()
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            emptyList()
        }
    }

    suspend fun obtenerNotasByAcitvidadId(id_actividad: Int, id_asignatura: Int): List<NotaDomain?> {
        return try {
            val response = api.getCalificaciones(id_actividad, id_asignatura)
            if (response.isSuccessful) {
                val re = response.body() ?: emptyList()
                re.map { it.toDomain() }
            } else {
                Log.e("ErrorResponse", "Error en la solicitud: ${response.code()}")
                emptyList()
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            emptyList()
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            emptyList()
        }
    }

    suspend fun obtenerNotasForStadicts(id_asignatura: Int): List<NotaDomain?> {
        return try {
            val response = api.getCalificacionesForStadicts(id_asignatura)
            if (response.isSuccessful) {
                val re = response.body() ?: emptyList()
                re.map { it.toDomain() }
            } else {
                Log.e("ErrorResponse", "Error en la solicitud: ${response.code()}")
                emptyList()
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            emptyList()
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            emptyList()
        }
    }

    suspend fun notaEditar(id: Int, notaE: NotaRe):Boolean {
        return try {
            withContext(Dispatchers.IO) {

                val res = api.editar_nota(id, notaE)

                val res2 = api.editar_nota(id, notaE)

                if (res.isSuccessful and  res2.isSuccessful){
                    Log.d("se logro", "se logro")
                    true
                } else {
                    Log.d("error", res.message())
                    false
                }
            }
        } catch (e: IOException) {
            Log.e("NetworkException", "Error de red: ${e.message}", e)
            false
        } catch (e: Exception) {
            Log.e("UnknownException", "Error desconocido: ${e.message}", e)
            false
        }
    }



    //TODO Database
    fun getAsignaturaByID(id: Int): Flow<AsignaturaEntity> {
        return userDao.getUserAsignaturaById(id)
    }

    fun getActividadByID(id: Int): Flow<ActividadEntity> {
        return userDao.getUserActividadById(id)
    }
    suspend fun clearCodeAsignature(){
        userDao.deleteAsignature()
    }
    fun getAsignatureDB(): Flow<List<AsignaturaEntity>?> {
        return userDao.getAsignatureUserWithFlow()
    }
    suspend fun insertsAsignature(asignaturas: List<AsignaturaEntity>){
        userDao.insertAsignature(asignaturas)
    }
    suspend fun insertsActivity(toDatabase: List<ActividadEntity>) {
        userDao.insertAllActivitys(toDatabase)
    }
    suspend fun insertUserStudent(toDatabase: UserStudentEntity){
        val lista = listOf(toDatabase)
        userDao.insertAllUserEs(lista)
    }
    suspend fun insertsNotas(toDatabase: List<NotaEntity>) {
        userDao.insertAllNotas(toDatabase)
    }
    suspend fun clearCodeStudent() {
        userDao.deleteStudentUser()
    }
    suspend fun clearCodeActivity() {
        userDao.deleteActivitiys()
    }
    suspend fun clearCodeNotas() {
        userDao.deleteAllNotes()
    }

    fun getStudentFromDatabaseWithFlow(): Flow<List<UserStudentEntity>> {
        return userDao.getStudentUserWithFlow()
    }
    fun getActivitiesDB(): Flow<List<ActividadEntity>> {
        return userDao.getActivitysUserWithFlow()
    }
    fun getAllNotes(): Flow<List<NotaEntity>> {
        return userDao.getNotasWithFlow()
    }


}