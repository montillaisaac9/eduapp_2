package com.example.eduapp.data.remote.dataModelsRE.crear
import com.google.gson.annotations.SerializedName

data class NewProfesorRe(
    @SerializedName("nombre") val nombre: String,
    @SerializedName("correo") val correo: String,
    @SerializedName("contraseña") val contraseña: String,
    @SerializedName("telefono") val telefono: String
)
