package com.example.eduapp.data.remote.dataModelsRE
import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.google.gson.annotations.SerializedName

data class AsignaturasRe(
    @SerializedName("id") val id: Int,
    @SerializedName("nombre") val nombre: String,
    @SerializedName("profesor_id") val profesorId: Int,
    @SerializedName("descripcion") val descripcion: String,
    @SerializedName("estudiantes_inscritos") val estudiantes: List<EstudianteInscrito>?,
    @SerializedName("token_pr") val token_pr: String?
)
