package com.example.eduapp.data.remote.dataModelsRE

import com.google.gson.annotations.SerializedName

data class NotaRe(

    @SerializedName("id") val id: Int,
    @SerializedName("estudiante_id") val studentId : String,
    @SerializedName("asignatura_id") val asignaturaId: Int,
    @SerializedName("actividad_id") val actividadId: Int,
    @SerializedName("puntuacion") val puntuacion: Int?,
    @SerializedName("fecha_calificacion") val fecha: String

) {
}



