package com.example.eduapp.data.remote.dataModelsRE
import com.google.gson.annotations.SerializedName


data class MateriaInscita(
    @SerializedName("id") val id: Int,
    @SerializedName("nombre") val nombre: String,
    @SerializedName("profesor_id") val profesorId: Int,
    @SerializedName("descripcion") val descripcion: String
)

data class StudentRE(
    @SerializedName("id") val id: String,
    @SerializedName("nombre") val nombre: String,
    @SerializedName("correo") val correo: String,
    @SerializedName("contraseña") val contraseña: String,
    @SerializedName("fecha_de_nacimiento") val fechaNacimiento: String,
    @SerializedName("sexo") val sexo: String,
    @SerializedName("foto") val foto: String,
    @SerializedName("telefono") val telefono: String,
    @SerializedName("latitud") val latitud: String,
    @SerializedName("longitud") val longitud: String,
    @SerializedName("materias_inscritas") val materias: List<MateriaInscita>
)
