package com.example.eduapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.eduapp.domain.model.ActividadDomain

@Entity(tableName = "Activitys")
data class ActividadEntity (

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "asignatura_id")
    val asignatura_id: Int,

    @ColumnInfo(name = "nombre")
    val nombre: String,

    @ColumnInfo(name = "descripcion")
    val descripcion: String,

    @ColumnInfo(name = "puntuacion_maxima")
    val puntuacion_Max: Int,

    @ColumnInfo(name = "fecha_limite")
    val fecha_limit: String,


)

fun ActividadEntity.toDomain() = ActividadDomain(id,asignatura_id,nombre,descripcion,puntuacion_Max,fecha_limit)
fun ActividadDomain.toDatabase() = ActividadEntity(id, asignatura_id, nombre, descripcion, puntuacion_Max, fecha_limit)
