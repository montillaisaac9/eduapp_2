package com.example.eduapp.data.remote.dataModelsRE.crear

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime


data class EstudianteInscrito(
    @SerializedName("id_estudiante") val id: String,
    @SerializedName("nombre_estudiante") val nombre: String,
    @SerializedName("edad_estudiante") val fecha: String,
    @SerializedName("sexo") val sexo: String,
    @SerializedName("Fecha_Inscripcion") val fechaIns: LocalDateTime,
    @SerializedName("token_es") val token: String?,
    val edad: Int?,
)


data class NewAsignaturaRe (
    @SerializedName("nombre") val nombre: String,
    @SerializedName("profesor_id") val profesorId: Int,
    @SerializedName("descripcion") val descripcion: String,
    @SerializedName("estudiantes_inscritos") val estudiantes: List<EstudianteInscrito>,
    @SerializedName("token_pr") val token_pr: String?
)