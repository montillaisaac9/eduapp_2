package com.example.eduapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.eduapp.domain.model.ProfesorDomain



@Entity(tableName = "user_profesor")
data class UserProfesorEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "nombre")
    val nombre: String,

    @ColumnInfo(name = "correo")
    val correo: String,

    @ColumnInfo(name = "contraseña")
    val contraseña: String,

    @ColumnInfo(name = "telefono")
    val telefono: String
)

fun ProfesorDomain.toDatabase () = UserProfesorEntity(id, nombre, correo, contraseña, telefono)