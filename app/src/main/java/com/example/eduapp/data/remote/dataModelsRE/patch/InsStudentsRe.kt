package com.example.eduapp.data.remote.dataModelsRE.patch

import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.google.gson.annotations.SerializedName

data class InsStudentsRe (
    @SerializedName("materias_inscritas") val materias: List<MateriaInscita>
)