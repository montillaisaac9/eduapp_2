package com.example.eduapp.data.remote.dataModelsRE.patch

import com.example.eduapp.data.remote.dataModelsRE.crear.EstudianteInscrito
import com.google.gson.annotations.SerializedName

data class InsAsignaturaRe(
    @SerializedName("estudiantes_inscritos") val estudiantes: List<EstudianteInscrito>
)
