package com.example.eduapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.eduapp.data.remote.dataModelsRE.MateriaInscita
import com.example.eduapp.domain.model.StudentDomain

@Entity(tableName = "user_student")
data class UserStudentEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "nombre")
    val nombre: String,

    @ColumnInfo(name = "correo")
    val correo: String,

    @ColumnInfo(name = "contraseña")
    val contraseña: String,

    @ColumnInfo(name = "fechaNacimiento")
    val fechaNacimiento: String,

    @ColumnInfo(name = "foto")
    val foto: String,

    @ColumnInfo(name = "sexo")
    val sexo: String,

    @ColumnInfo(name = "telefono")
    val telefono: String,

    @ColumnInfo(name = "latitud")
    val latitud: String,

    @ColumnInfo(name = "longitud")
    val longitud: String,

    @ColumnInfo(name = "materias")
    val materias: List<MateriaInscita>
)

fun StudentDomain.toDatabase () = UserStudentEntity(id, nombre, correo, contraseña, fechaNacimiento, foto, sexo, telefono, latitud, longitud, materias)