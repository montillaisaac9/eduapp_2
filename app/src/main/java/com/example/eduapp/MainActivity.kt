package com.example.eduapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.eduapp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

private lateinit var bingding: ActivityMainBinding


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bingding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bingding.root)
    }

    override fun onDestroy() {
        super.onDestroy()

    }
}
