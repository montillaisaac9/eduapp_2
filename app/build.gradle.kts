plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
    id ("com.google.gms.google-services")
}

android {
    namespace = "com.example.eduapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.eduapp"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    kapt {
        correctErrorTypes = true
    }
   // kotlin {
  //     jvmToolchain(8)
   //}
}

dependencies {
    implementation("androidx.compose.material3:material3-android:1.2.1")
    val nav_version = "2.7.7"
    val room_version = "2.6.1"
    val composeVersion = "1.6.4"



    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    //Navigation XML
    implementation("androidx.navigation:navigation-fragment-ktx:2.7.7")
    implementation("androidx.navigation:navigation-ui-ktx:2.7.7")
    //Navigation Compose
    implementation("androidx.navigation:navigation-compose:$nav_version")
    //NavSafe
    implementation("androidx.navigation:navigation-fragment-ktx:$nav_version")
    implementation("androidx.navigation:navigation-ui-ktx:$nav_version")
    //Fragment
    implementation("androidx.fragment:fragment-ktx:1.6.2")
    // Compose
    implementation(platform("androidx.compose:compose-bom:2024.02.02"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.material:material")
    implementation("androidx.compose.runtime:runtime")
    implementation("androidx.activity:activity-compose:1.8.2")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.ui:ui-tooling:$composeVersion") // Dependencia principal de Compose
    implementation("androidx.compose.ui:ui-tooling-preview:$composeVersion") // Dependencia para previsualización
    implementation("androidx.lifecycle:lifecycle-runtime-compose:2.6.2")
    implementation ("io.coil-kt:coil-compose:1.3.2")
    //Glide
    implementation ("com.github.bumptech.glide:glide:4.16.0")
    //Retrofic
    implementation ("com.squareup.retrofit2:retrofit:2.10.0")
    implementation ("com.squareup.retrofit2:converter-gson:2.10.0")
    //Courutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.9")
    //ROOM
    implementation("androidx.room:room-ktx:$room_version")
    kapt("androidx.room:room-compiler:$room_version")
    //dagger hill
    implementation("com.google.dagger:hilt-android:2.44")
    kapt("com.google.dagger:hilt-android-compiler:2.44")
    //obtener ubicacion
    implementation ("com.google.android.gms:play-services-location:21.2.0")
    //fechas
    implementation ("com.jakewharton.threetenabp:threetenabp:1.3.0")
    //estadisticas
    implementation ("com.github.tehras:charts:0.2.4-alpha")
    //maps
    implementation ("com.google.maps.android:maps-compose:2.11.4")
    implementation ("com.google.android.gms:play-services-maps:18.1.0")
    //firebase
    implementation (platform("com.google.firebase:firebase-bom:32.8.1"))
    implementation ("com.google.firebase:firebase-analytics")
    implementation("com.google.firebase:firebase-messaging-ktx:21.2.1")

    //aaa
    // Compose dependencies
    implementation ("androidx.activity:activity-compose:1.8.0") // O la versión más reciente de `activity-compose`
    implementation ("androidx.core:core-ktx:1.10.0") // O la versión más reciente de `core-ktx`
    implementation ("androidx.activity:activity-ktx:1.8.0")

}
